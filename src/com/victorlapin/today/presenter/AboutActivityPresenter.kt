package com.victorlapin.today.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.victorlapin.today.Screens
import com.victorlapin.today.view.AboutActivityView
import ru.terrakok.cicerone.Router

@InjectViewState
class AboutActivityPresenter(
        private val mRouter: Router
) : MvpPresenter<AboutActivityView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showFragment()
    }

    private fun showFragment() = mRouter.replaceScreen(Screens.FRAGMENT_ABOUT)

    fun onBackPressed() = mRouter.exit()
}