package com.victorlapin.today.presenter

import android.content.SharedPreferences
import android.net.Uri
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.victorlapin.today.Screens
import com.victorlapin.today.addTo
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.interactor.BeansInteractor
import com.victorlapin.today.model.interactor.NotesInteractor
import com.victorlapin.today.view.TodayFragmentView
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router

@InjectViewState
class TodayFragmentPresenter(
        private val mRouter: Router,
        private val mBeansInteractor: BeansInteractor,
        private val mSettings: SettingsManager,
        private val mNotesInteractor: NotesInteractor
) : MvpPresenter<TodayFragmentView>() {
    private var mHasData: Boolean = false
    private var mDisposable: CompositeDisposable? = null

    private val mListener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
        when (key) {
            SettingsManager.KEY_FEED_PROVIDERS -> reloadBeans()
            SettingsManager.KEY_WEATHER_UNITS,
            SettingsManager.KEY_WEATHER_SHOW_FORECAST -> loadWeather()
            SettingsManager.KEY_CURRENCIES_BASES,
            SettingsManager.KEY_CURRENCIES_SYMBOL -> loadCurrencies()
            SettingsManager.KEY_CALLS_COUNT,
            SettingsManager.KEY_CALLS_TYPES -> loadCalls()
            SettingsManager.KEY_SEARCH_ENGINE -> loadMeta()
            SettingsManager.KEY_CRYPTO_SYMBOLS_FROM,
            SettingsManager.KEY_CRYPTO_SYMBOL_TO -> loadCryptoCurrencies()
        }
    }

    override fun attachView(view: TodayFragmentView?) {
        super.attachView(view)
        mDisposable = CompositeDisposable()
        mSettings.bindListener(mListener)
        reloadBeans()
    }

    override fun detachView(view: TodayFragmentView?) {
        mSettings.unbindListener(mListener)
        mDisposable?.clear()
        mDisposable = null
        super.detachView(view)
    }

    fun reloadBeans() {
        viewState.showRefresh()
        viewState.onClearBeans()
        mHasData = false
        mBeansInteractor
                .loadBeans(DateBuilder())
                .doOnComplete {
                    viewState.hideRefresh()
                    if (!mHasData) {
                        viewState.showListEmpty()
                    }
                }
                .subscribe { bean ->
                    if (bean.isDataBean && !mHasData) {
                        mHasData = true
                        viewState.hideListEmpty()
                    }
                    viewState.onNextBean(bean)
                }
                .addTo(mDisposable!!)
    }

    private fun loadWeather() {
        viewState.onClearBeans("weather")
        mBeansInteractor.loadWeather(DateBuilder())
                .subscribe { viewState.onNextBean(it) }
                .addTo(mDisposable!!)
    }

    private fun loadCurrencies() {
        viewState.onClearBeans("currencies")
        mBeansInteractor.loadCurrencies(DateBuilder())
                .subscribe { viewState.onNextBean(it) }
                .addTo(mDisposable!!)
    }

    private fun loadCalls() {
        viewState.onClearBeans("calls")
        mBeansInteractor.loadCalls(DateBuilder())
                .subscribe { viewState.onNextBean(it) }
                .addTo(mDisposable!!)
    }

    private fun loadMeta() {
        mBeansInteractor.loadMeta(DateBuilder())
                .subscribe { viewState.onNextBean(it) }
                .addTo(mDisposable!!)
    }

    private fun loadCryptoCurrencies() {
        viewState.onClearBeans("crypto_currencies")
        mBeansInteractor.loadCryptoCurrencies(DateBuilder())
                .subscribe { viewState.onNextBean(it) }
                .addTo(mDisposable!!)
    }

    fun onBeanClicked(args: BeanClickEventArgs) {
        when (args.beanType) {
            Bean.BEAN_TYPE_SIGN_IN_GOOGLE -> viewState.signInWithGoogle()
            Bean.BEAN_TYPE_CALENDAR ->
                mRouter.navigateTo(Screens.EXTERNAL_CALENDAR, (args.data as Long))
            Bean.BEAN_TYPE_CALLS ->
                mRouter.navigateTo(Screens.EXTERNAL_CONTACT, (args.data as Uri))
            Bean.BEAN_TYPE_PERMISSIONS -> viewState.requestPermissions(args.data as Array<String>)
            Bean.BEAN_TYPE_PERMISSIONS_OAUTH -> viewState.requestFitnessAccess()
            Bean.BEAN_TYPE_WEARABLES ->
                mRouter.navigateTo(Screens.EXTERNAL_BLUETOOTH)
            Bean.BEAN_TYPE_SEARCH ->
                mRouter.navigateTo(Screens.EXTERNAL_SEARCH, args.data)
            Bean.BEAN_TYPE_NOTES_PLACEHOLDER -> {
                mRouter.setResultListener(Screens.EXIT_CODE_NOTE) {
                    it?.let {
                        viewState.onClearBeans("notes")
                        reloadBeans()
                    }
                    mRouter.removeResultListener(Screens.EXIT_CODE_NOTE)
                }
                mRouter.navigateTo(Screens.ACTIVITY_NOTE)
            }

        }
    }

    fun onMenuClicked(args: MenuClickEventArgs) {
        when (args.eventType) {
            EventType.NAVIGATE -> mRouter.navigateTo(args.screenKey, args.data)
            EventType.DISABLE_PROVIDER -> {
                if (mSettings.disableProvider(args.data.toString())) {
                    viewState.onProviderDisabled(args.data.toString())
                }
            }
            EventType.ENABLE_PROVIDER -> mSettings.enableProvider(args.data.toString())
            EventType.DELETE_NOTE -> {
                mNotesInteractor.deleteNote(args.data as Long)
                viewState.onClearBeans("notes")
                reloadBeans()
            }
            EventType.EDIT_NOTE -> {
                mRouter.setResultListener(Screens.EXIT_CODE_NOTE) {
                    it?.let {
                        reloadBeans()
                    }
                    mRouter.removeResultListener(Screens.EXIT_CODE_NOTE)
                }
                mRouter.navigateTo(Screens.ACTIVITY_NOTE, args.data as Long)
            }
        }
    }

    fun onRequestPermissionsResult() = reloadBeans()

    fun onSignInResult() = reloadBeans()
}