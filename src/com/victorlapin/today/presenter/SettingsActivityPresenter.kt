package com.victorlapin.today.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.victorlapin.today.Screens
import com.victorlapin.today.view.SettingsActivityView
import ru.terrakok.cicerone.Router

@InjectViewState
class SettingsActivityPresenter(
        private val mRouter: Router
) : MvpPresenter<SettingsActivityView>() {
    fun showFragment(screenKey: String) = mRouter.replaceScreen(screenKey)

    fun onBackPressed() = mRouter.exit()

    fun openAssistantSystemSettings() = mRouter.navigateTo(Screens.EXTERNAL_ASSISTANT)
}