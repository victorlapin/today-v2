package com.victorlapin.today.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.victorlapin.today.Screens
import com.victorlapin.today.view.TodayActivityView
import ru.terrakok.cicerone.Router

@InjectViewState
class TodayActivityPresenter(
        private val mRouter: Router
) : MvpPresenter<TodayActivityView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        mRouter.replaceScreen(Screens.FRAGMENT_TODAY)
    }
}