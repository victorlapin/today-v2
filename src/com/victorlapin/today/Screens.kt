package com.victorlapin.today

object Screens {
    const val ACTIVITY_TODAY = "activity_today"
    const val ACTIVITY_SETTINGS = "activity_settings"
    const val ACTIVITY_ABOUT = "activity_about"
    const val ACTIVITY_LICENSES = "activity_licenses"
    const val ACTIVITY_NOTE = "activity_note"

    const val FRAGMENT_TODAY = "fragment_today"
    const val FRAGMENT_ABOUT = "fragment_about"
    const val FRAGMENT_SETTINGS_GLOBAL = "fragment_settings_global"
    const val FRAGMENT_SETTINGS_WEATHER = "fragment_settings_weather"
    const val FRAGMENT_SETTINGS_CURRENCIES = "fragment_settings_currencies"
    const val FRAGMENT_SETTINGS_CALLS = "fragment_settings_calls"
    const val FRAGMENT_SETTINGS_SEARCH = "fragment_settings_search"
    const val FRAGMENT_SETTINGS_CRYPTO_CURRENCIES = "fragment_settings_crypto_currencies"
    const val FRAGMENT_NOTE = "fragment_note"

    const val EXTERNAL_CALENDAR = "calendar"
    const val EXTERNAL_CONTACT = "contact"
    const val EXTERNAL_BLUETOOTH = "bluetooth"
    const val EXTERNAL_SEARCH = "search"
    const val EXTERNAL_ABOUT = "about"
    const val EXTERNAL_ASSISTANT = "assistant"

    const val EXIT_CODE_NOTE = 0
}