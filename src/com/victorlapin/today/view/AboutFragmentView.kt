package com.victorlapin.today.view

import com.arellomobile.mvp.MvpView
import com.victorlapin.today.model.repository.AboutRepository

interface AboutFragmentView: MvpView {
    fun setData(data: List<AboutRepository.ListItem>)
}