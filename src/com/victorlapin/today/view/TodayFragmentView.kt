package com.victorlapin.today.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.victorlapin.today.model.bean.Bean

interface TodayFragmentView: MvpView {
    @StateStrategyType(SkipStrategy::class)
    fun showRefresh()
    @StateStrategyType(SkipStrategy::class)
    fun hideRefresh()
    fun onClearBeans(vararg providers: String)
    fun onNextBean(bean: Bean)
    fun showListEmpty()
    fun hideListEmpty()
    fun requestPermissions(permissions: Array<String>)
    fun requestFitnessAccess()
    @StateStrategyType(SkipStrategy::class)
    fun signInWithGoogle()
    @StateStrategyType(SkipStrategy::class)
    fun onProviderDisabled(provider: String)
}