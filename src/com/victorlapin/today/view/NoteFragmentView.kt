package com.victorlapin.today.view

import com.arellomobile.mvp.MvpView
import com.victorlapin.today.model.database.entity.Note

interface NoteFragmentView : MvpView {
    fun setData(note: Note)
}