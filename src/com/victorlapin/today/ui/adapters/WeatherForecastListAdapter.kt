package com.victorlapin.today.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.bean.WeatherBean
import kotlinx.android.synthetic.main.item_weather_forecast.view.*

class WeatherForecastListAdapter(val items: List<WeatherBean.Forecast>) :
        RecyclerView.Adapter<WeatherForecastListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.item_weather_forecast))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.itemView.lbl_date.text = item.date
        holder.itemView.lbl_temp.text = "${item.temp?.let {
            (if (it > 1) "+" else "") + "%.0f".format(it) + "°" + item.tempType
        }}"
        if (item.image != null) {
            holder.itemView.image.setImageDrawable(item.image)
            holder.itemView.image.visibility = View.VISIBLE
        } else {
            holder.itemView.image.visibility = View.INVISIBLE
        }
    }

    override fun getItemCount(): Int = items.size
}