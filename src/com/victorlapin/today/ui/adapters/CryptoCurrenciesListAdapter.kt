package com.victorlapin.today.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.model.bean.CryptoCurrenciesBean
import kotlinx.android.synthetic.main.item_crypto_currencies_row.view.*

class CryptoCurrenciesListAdapter(
        private val mItems: List<CryptoCurrenciesBean.Item>,
        private val mResources: ResourcesManager
) : RecyclerView.Adapter<CryptoCurrenciesListAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.item_crypto_currencies_row))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mItems[position]
        holder.itemView.lbl_description.text = item.value
        if (item.imageRes != null) {
            holder.itemView.image.setImageDrawable(mResources.getDrawable(item.imageRes))
        } else {
            holder.itemView.image.setImageDrawable(null)
        }
    }

    override fun getItemCount(): Int = mItems.size
}