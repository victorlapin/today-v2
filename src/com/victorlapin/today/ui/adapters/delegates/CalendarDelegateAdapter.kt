package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.CalendarBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_footer.view.*
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_calendar.view.*

class CalendarDelegateAdapter(
        private val mClickEvent: PublishSubject<BeanClickEventArgs>,
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = CalendarViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as CalendarViewHolder).bind(item as CalendarBean)
    }

    inner class CalendarViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_calendar)) {
        private val allDayString =
                itemView.context.getString(R.string.provider_calendar_all_day_event)
        private val durationString =
                itemView.context.getString(R.string.provider_calendar_duration_template)

        init {
            itemView.btn_customize.setOnClickListener { showPopup() }
        }

        fun bind(bean: CalendarBean) {
            itemView.lbl_header.text = bean.cardHeader
            itemView.lbl_source.text = bean.source
            itemView.lbl_title.text = bean.title

            if (bean.isAllDayEvent!!) {
                itemView.img_time.visibility = View.VISIBLE
                itemView.lbl_time.visibility = View.VISIBLE
                itemView.lbl_time.text = allDayString
            } else if (!TextUtils.isEmpty(bean.dateStart) && !TextUtils.isEmpty(bean.dateEnd)) {
                itemView.img_time.visibility = View.VISIBLE
                itemView.lbl_time.visibility = View.VISIBLE
                itemView.lbl_time.text = durationString.format(bean.dateStart, bean.dateEnd)
            } else {
                itemView.img_time.visibility = View.GONE
                itemView.lbl_time.visibility = View.GONE
            }

            if (!TextUtils.isEmpty(bean.location)) {
                itemView.img_location.visibility = View.VISIBLE
                itemView.lbl_location.visibility = View.VISIBLE
                itemView.lbl_location.text = bean.location
            } else {
                itemView.img_location.visibility = View.GONE
                itemView.lbl_location.visibility = View.GONE
            }

            if (!TextUtils.isEmpty(bean.description)) {
                itemView.img_description.visibility = View.VISIBLE
                itemView.lbl_description.visibility = View.VISIBLE
                itemView.lbl_description.text = bean.description
            } else {
                itemView.img_description.visibility = View.GONE
                itemView.lbl_description.visibility = View.GONE
            }

            bean.eventId?.let {
                itemView.container.setOnClickListener {
                    mClickEvent.onNext(BeanClickEventArgs(bean.beanType, bean.eventId))
                }
            }
        }

        private fun showPopup() {
            val popupMenu = PopupMenu(itemView.context,
                    itemView.anchor)
            popupMenu.inflate(R.menu.item_context_disable_only)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_context_disable -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DISABLE_PROVIDER,
                                null,
                                "calendar"))
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}