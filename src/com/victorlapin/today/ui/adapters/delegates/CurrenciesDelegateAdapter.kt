package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.inflate
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.CurrenciesBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_footer.view.*
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_currencies.view.*

class CurrenciesDelegateAdapter(
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = CurrenciesViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as CurrenciesViewHolder).bind(item as CurrenciesBean)
    }

    inner class CurrenciesViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_currencies)) {
        private val mSourceString =
                itemView.context.getString(R.string.provider_source_template)

        init {
            itemView.btn_customize.setOnClickListener { showPopup() }
        }

        fun bind(bean: CurrenciesBean) {
            val sb = StringBuilder()

            for (str in bean.items) {
                if (sb.isNotEmpty()) {
                    sb.append("\n")
                }
                sb.append(str)
            }
            itemView.lbl_description.text = sb.toString()
            itemView.lbl_header.text = bean.cardHeader
            itemView.lbl_source.text =
                    mSourceString.format(bean.source, bean.lastUpdateTime)
        }

        private fun showPopup() {
            val popupMenu = PopupMenu(itemView.context,
                    itemView.anchor)
            popupMenu.inflate(R.menu.item_context_full)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_context_customize -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.NAVIGATE,
                                Screens.ACTIVITY_SETTINGS,
                                Screens.FRAGMENT_SETTINGS_CURRENCIES))
                        true
                    }
                    R.id.action_context_disable -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DISABLE_PROVIDER,
                                null,
                                "currencies"))
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}