package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.WearablesBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_footer.view.*
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_wearables.view.*

class WearablesDelegateAdapter(
        private val mClickEvent: PublishSubject<BeanClickEventArgs>,
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = WearablesViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as WearablesViewHolder).bind(item as WearablesBean)
    }

    inner class WearablesViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_wearables)) {

        init {
            itemView.btn_customize.setOnClickListener { showPopup() }
        }

        fun bind(bean: WearablesBean) {
            val sb = StringBuilder()

            for (str in bean.devices) {
                if (sb.isNotEmpty()) {
                    sb.append("\n")
                }
                sb.append(str)
            }
            itemView.lbl_description.text = sb.toString()
            itemView.lbl_header.text = bean.cardHeader
            itemView.lbl_source.text = bean.source

            itemView.container.setOnClickListener {
                mClickEvent.onNext(BeanClickEventArgs(bean.beanType, null))
            }
        }

        private fun showPopup() {
            val popupMenu = PopupMenu(itemView.context,
                    itemView.anchor)
            popupMenu.inflate(R.menu.item_context_disable_only)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_context_disable -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DISABLE_PROVIDER,
                                null,
                                "wearables"))
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}