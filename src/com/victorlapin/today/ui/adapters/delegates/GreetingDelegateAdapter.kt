package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.GreetingBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import kotlinx.android.synthetic.main.item_greeting.view.*

class GreetingDelegateAdapter : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = GreetingViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as GreetingViewHolder).bind(item as GreetingBean)
    }

    inner class GreetingViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_greeting)) {
        fun bind(bean: GreetingBean) {
            itemView.lbl_today.text = bean.title
            if (bean.accountName != null) {
                itemView.lbl_account_name.visibility = View.VISIBLE
                itemView.lbl_account_name.text = itemView.context
                        .getString(R.string.greeting, bean.accountName)
            } else {
                itemView.lbl_account_name.visibility = View.GONE
            }
        }
    }
}