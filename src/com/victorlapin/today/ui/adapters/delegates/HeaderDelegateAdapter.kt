package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import com.victorlapin.today.util.Dimensions

class HeaderDelegateAdapter: BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = HeaderViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as HeaderViewHolder).bind()
    }

    inner class HeaderViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_header)) {

        fun bind() {
            itemView.minimumHeight = Dimensions.statusBarHeight
        }
    }
}