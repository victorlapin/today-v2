package com.victorlapin.today.ui.adapters.delegates

import android.annotation.SuppressLint
import android.net.Uri
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.inflate
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.CallsBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import com.victorlapin.today.ui.adapters.CallsListAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_calls.view.*

class CallsDelegateAdapter(
        private val mClickEvent: PublishSubject<BeanClickEventArgs>,
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>,
        private val mResources: ResourcesManager
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = CallsViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as CallsViewHolder).bind(item as CallsBean)
    }

    inner class CallsViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_calls)) {

        init {
            itemView.btn_customize.setOnClickListener { showPopup() }
        }

        @SuppressLint("CheckResult")
        fun bind(bean: CallsBean) {
            itemView.lbl_header.text = bean.cardHeader
            itemView.list_log.layoutManager = LinearLayoutManager(itemView.context)
            itemView.list_log.setHasFixedSize(true)
            if (bean.items.isNotEmpty()) {
                val logClickEvent = PublishSubject.create<Uri>()
                logClickEvent.subscribe {
                    mClickEvent.onNext(BeanClickEventArgs(bean.beanType, it))
                }
                itemView.empty_view.visibility = View.GONE
                itemView.list_log.adapter = CallsListAdapter(bean.items, logClickEvent,
                        mResources)

            } else {
                itemView.empty_view.visibility = View.VISIBLE
                itemView.list_log.adapter = null
            }
        }

        private fun showPopup() {
            val popupMenu = PopupMenu(itemView.context,
                    itemView.anchor)
            popupMenu.inflate(R.menu.item_context_full)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_context_customize -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.NAVIGATE,
                                Screens.ACTIVITY_SETTINGS,
                                Screens.FRAGMENT_SETTINGS_CALLS))
                        true
                    }
                    R.id.action_context_disable -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DISABLE_PROVIDER,
                                null,
                                "calls"))
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}