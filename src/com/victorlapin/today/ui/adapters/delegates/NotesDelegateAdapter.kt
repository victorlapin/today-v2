package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.NoteBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_note.view.*

class NotesDelegateAdapter(
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = NoteViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as NoteViewHolder).bind(item as NoteBean)
    }

    inner class NoteViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_note)) {
        private var mNoteId: Long? = null

        init {
            itemView.btn_customize.setOnClickListener { showPopup() }
        }

        fun bind(bean: NoteBean) {
            mNoteId = bean.noteId
            itemView.lbl_header.text = bean.cardHeader
            itemView.lbl_text.text = bean.text
        }

        private fun showPopup() {
            val popupMenu = PopupMenu(itemView.context,
                    itemView.anchor)
            popupMenu.inflate(R.menu.item_context_note)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_context_edit -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.EDIT_NOTE,
                                null,
                                mNoteId))
                        true
                    }
                    R.id.action_context_delete -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DELETE_NOTE,
                                null,
                                mNoteId))
                        true
                    }
                    R.id.action_context_disable -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DISABLE_PROVIDER,
                                null,
                                "notes"))
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}