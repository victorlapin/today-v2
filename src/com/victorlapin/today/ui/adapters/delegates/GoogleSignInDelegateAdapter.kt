package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.GoogleSignInBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_sign_in_google.view.*

class GoogleSignInDelegateAdapter(
        private val mClickEvent: PublishSubject<BeanClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = GoogleSignInViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as GoogleSignInViewHolder).bind(item as GoogleSignInBean)
    }

    inner class GoogleSignInViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_sign_in_google)) {

        fun bind(bean: GoogleSignInBean) {
            itemView.container.setOnClickListener {
                mClickEvent.onNext(BeanClickEventArgs(bean.beanType, null))
            }
        }
    }
}