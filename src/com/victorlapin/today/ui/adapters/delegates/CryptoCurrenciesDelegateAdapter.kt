package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.inflate
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.CryptoCurrenciesBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import com.victorlapin.today.ui.adapters.CryptoCurrenciesListAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_footer.view.*
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_crypto_currencies.view.*

class CryptoCurrenciesDelegateAdapter(
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>,
        private val mResources: ResourcesManager
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = CryptoViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as CryptoViewHolder).bind(item as CryptoCurrenciesBean)
    }

    inner class CryptoViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_crypto_currencies)) {
        private val mSourceString =
                itemView.context.getString(R.string.provider_source_template)

        init {
            itemView.btn_customize.setOnClickListener { showPopup() }
        }

        fun bind(bean: CryptoCurrenciesBean) {
            itemView.lbl_header.text = bean.cardHeader
            itemView.lbl_source.text =
                    mSourceString.format(bean.source, bean.lastUpdateTime)
            itemView.list_items.apply {
                layoutManager = LinearLayoutManager(itemView.context)
                setHasFixedSize(true)
                adapter = CryptoCurrenciesListAdapter(bean.items, mResources)
            }
        }

        private fun showPopup() {
            val popupMenu = PopupMenu(itemView.context,
                    itemView.anchor)
            popupMenu.inflate(R.menu.item_context_full)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_context_customize -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.NAVIGATE,
                                Screens.ACTIVITY_SETTINGS,
                                Screens.FRAGMENT_SETTINGS_CRYPTO_CURRENCIES))
                        true
                    }
                    R.id.action_context_disable -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DISABLE_PROVIDER,
                                null,
                                "crypto_currencies"))
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}