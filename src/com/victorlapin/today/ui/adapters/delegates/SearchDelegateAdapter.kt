package com.victorlapin.today.ui.adapters.delegates

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.TextView
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.inflate
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.SearchBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import com.victorlapin.today.ui.adapters.SearchArrayAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_search.view.*
import java.util.ArrayList

class SearchDelegateAdapter(
        private val mClickEvent: PublishSubject<BeanClickEventArgs>,
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = SearchViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as SearchViewHolder).bind(item as SearchBean)
    }

    inner class SearchViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_search)) {
        private val autocompleteAdapter: ArrayAdapter<String>

        init {
            itemView.btn_customize.visibility = View.VISIBLE
            itemView.btn_customize.setOnClickListener {
                mMenuEvent.onNext(MenuClickEventArgs(EventType.NAVIGATE,
                        Screens.ACTIVITY_SETTINGS,
                        Screens.FRAGMENT_SETTINGS_SEARCH))
            }

            autocompleteAdapter = SearchArrayAdapter(
                    itemView.context, ArrayList(),
                    itemView.search.currentTextColor, itemView.btn_customize.imageTintList)
            itemView.search.setAdapter<ArrayAdapter<String>>(autocompleteAdapter)
        }

        fun bind(bean: SearchBean) {
            itemView.search.hint = bean.title
            itemView.search.setOnEditorActionListener(
                    TextView.OnEditorActionListener { _, actionId, keyEvent ->
                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                keyEvent != null && keyEvent.keyCode == KeyEvent.KEYCODE_ENTER
                                        && keyEvent.action == KeyEvent.ACTION_DOWN) {
                            val query = itemView.search.text.toString()
                            if (autocompleteAdapter.getPosition(query) < 0) {
                                autocompleteAdapter.add(query)
                                autocompleteAdapter.notifyDataSetChanged()
                            }
                            itemView.search.text = null

                            val bundle = Bundle()
                            bundle.apply {
                                putString(SearchBean.EXTRA_QUERY, query)
                                putString(SearchBean.EXTRA_APP_PACKAGE, bean.appPackage)
                                putString(SearchBean.EXTRA_URL, bean.url)
                            }
                            mClickEvent.onNext(BeanClickEventArgs(bean.beanType, bundle))
                            return@OnEditorActionListener true
                        }
                        false
                    })
        }
    }
}