package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.inflate
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_toolbar.view.*

class ToolbarDelegateAdapter(
        private val mMenuClick: PublishSubject<MenuClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = ToolbarViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {

    }

    inner class ToolbarViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_toolbar)) {
        init {
            itemView.toolbar.inflateMenu(R.menu.activity_today)
            itemView.toolbar.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.action_settings -> {
                        mMenuClick.onNext(MenuClickEventArgs(EventType.NAVIGATE,
                                Screens.ACTIVITY_SETTINGS,
                                Screens.FRAGMENT_SETTINGS_GLOBAL))
                        true
                    }
                    R.id.action_about -> {
                        mMenuClick.onNext(MenuClickEventArgs(EventType.NAVIGATE,
                                Screens.ACTIVITY_ABOUT,
                                null))
                        true
                    }
                    else -> false
                }
            }
        }
    }
}