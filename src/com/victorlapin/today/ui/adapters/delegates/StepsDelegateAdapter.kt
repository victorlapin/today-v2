package com.victorlapin.today.ui.adapters.delegates

import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.R
import com.victorlapin.today.inflate
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.StepsBean
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.include_item_footer.view.*
import kotlinx.android.synthetic.main.include_item_header.view.*
import kotlinx.android.synthetic.main.item_steps.view.*

class StepsDelegateAdapter(
        private val mMenuEvent: PublishSubject<MenuClickEventArgs>
) : BeanDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup) = StepsViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean) {
        (holder as StepsViewHolder).bind(item as StepsBean)
    }

    inner class StepsViewHolder(parent: ViewGroup) :
            RecyclerView.ViewHolder(parent.inflate(R.layout.item_steps)) {
        private val mStepsCountString =
                itemView.context.getString(R.string.provider_fitness_steps_count)
        private val mStepsAdditionalString =
                itemView.context.getString(R.string.provider_fitness_steps_additional)
        private val mSourceString =
                itemView.context.getString(R.string.provider_source_template)

        init {
            itemView.btn_customize.setOnClickListener { showPopup() }
        }

        fun bind(bean: StepsBean) {
            itemView.lbl_header.text = bean.cardHeader
            itemView.lbl_source.text =
                    mSourceString.format(bean.source, bean.lastUpdateTime)
            itemView.lbl_steps.text = mStepsCountString.format(bean.steps)
            itemView.lbl_additional.text =
                    mStepsAdditionalString.format(bean.distance, bean.calories)
        }

        private fun showPopup() {
            val popupMenu = PopupMenu(itemView.context,
                    itemView.anchor)
            popupMenu.inflate(R.menu.item_context_disable_only)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_context_disable -> {
                        mMenuEvent.onNext(MenuClickEventArgs(EventType.DISABLE_PROVIDER,
                                null,
                                "fitness"))
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}