package com.victorlapin.today.ui.adapters

import android.content.Context
import android.content.res.ColorStateList
import android.support.constraint.ConstraintLayout
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.victorlapin.today.R

class SearchArrayAdapter(context: Context, items: List<String>,
                         private val mTextColor: Int, private val mImageTintList: ColorStateList) :
        ArrayAdapter<String>(context, R.layout.item_search_autocomplete, R.id.text, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = super.getView(position, convertView, parent)

        val rootView = view.findViewById<View>(R.id.click_container) as ConstraintLayout
        val picture = rootView.getChildAt(0) as ImageView
        picture.imageTintList = mImageTintList
        val text1 = rootView.getChildAt(1) as TextView
        text1.setTextColor(mTextColor)

        return view
    }
}
