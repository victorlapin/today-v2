package com.victorlapin.today.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.bean.Bean
import kotlin.collections.ArrayList

class TodayAdapter(
        private val mDelegates: HashMap<Int, BeanDelegateAdapter>,
        private val mSettings: SettingsManager
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mItems = ArrayList<Bean>()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            mDelegates[viewType]!!.onCreateViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val bean = mItems[position]
        val viewType = getItemViewType(position)
        mDelegates[viewType]!!.onBindViewHolder(holder, bean)
    }

    override fun getItemCount() = mItems.size

    override fun getItemViewType(position: Int) = mItems[position].beanType

    override fun getItemId(position: Int) = mItems[position].id

    fun clear(vararg providers: String) {
        val beansToRemove = ArrayList<Bean>()
        if (providers.isNotEmpty()) {
            mItems
                    .filterTo(beansToRemove) {
                        providers.contains(it.providerName)
                    }
        } else {
            mItems
                    .filterTo(beansToRemove) {
                        (it.isDataBean && !mSettings.isProviderEnabled(it.providerName))
                                || it.isPolymorphBean
                    }
        }
        if (beansToRemove.isNotEmpty()) {
            mItems.removeAll(beansToRemove)
            notifyDataSetChanged()
        }
    }

    fun add(element: Bean) {
        for (i in 0 until mItems.size) {
            if (mItems[i].id == element.id) {
                mItems[i] = element
                notifyItemChanged(i)
                return
            }
            if (mItems[i].id > element.id) {
                mItems.add(i, element)
                notifyItemInserted(i)
                return
            }
        }
        mItems.add(element)
        notifyItemInserted(mItems.size - 1)
    }
}