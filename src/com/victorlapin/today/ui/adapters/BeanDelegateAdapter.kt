package com.victorlapin.today.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.victorlapin.today.model.bean.Bean

interface BeanDelegateAdapter {
    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
    fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: Bean)
}