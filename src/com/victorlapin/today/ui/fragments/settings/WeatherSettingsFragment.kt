package com.victorlapin.today.ui.fragments.settings

import android.os.Bundle
import android.support.v7.preference.ListPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import com.victorlapin.today.R
import com.victorlapin.today.manager.SettingsManager
import org.koin.android.ext.android.inject

class WeatherSettingsFragment : PreferenceFragmentCompat() {
    private val mSettings by inject<SettingsManager>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_weather)

        val unitPreference =
                findPreference(SettingsManager.KEY_WEATHER_UNITS) as ListPreference
        val value = mSettings.weatherUnits
        val entries = unitPreference.entries
        val index = unitPreference.findIndexOfValue(value)
        unitPreference.value = value
        unitPreference.summary = entries[index]
        unitPreference.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { preference, newValue ->
                    val mPreference = preference as ListPreference
                    val mIndex = mPreference.findIndexOfValue(newValue.toString())
                    preference.summary = entries[mIndex]
                    true
                }
    }
}