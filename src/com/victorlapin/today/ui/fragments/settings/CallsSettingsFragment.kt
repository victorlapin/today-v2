package com.victorlapin.today.ui.fragments.settings

import android.os.Bundle
import android.support.v14.preference.MultiSelectListPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import android.util.ArraySet
import com.victorlapin.today.R
import com.victorlapin.today.manager.SettingsManager
import org.koin.android.ext.android.inject

class CallsSettingsFragment : PreferenceFragmentCompat() {
    private val mSettings by inject<SettingsManager>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_calls)

        val typesPreference =
                findPreference(SettingsManager.KEY_CALLS_TYPES) as MultiSelectListPreference
        val values = mSettings.callsTypes
        typesPreference.values = values
        typesPreference.summary = getSummary(typesPreference.entries,
                typesPreference.entryValues, values)
        typesPreference.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { preference, newValue ->
                    val pref = preference as MultiSelectListPreference
                    pref.summary = getSummary(pref.entries, pref.entryValues,
                            newValue as Set<*>)
                    true
                }
    }

    private fun getSummary(entries: Array<CharSequence>, entryValues: Array<CharSequence>,
                           values: Set<*>): String {
        val result = ArraySet<String>()
        values.forEach {
            val index = entryValues.indexOf(it)
            if (index >= 0) {
                result.add(entries[index].toString())
            }
        }
        return mSettings.flatten(result)
    }
}