package com.victorlapin.today.ui.fragments.settings

import android.os.Bundle
import android.support.v14.preference.MultiSelectListPreference
import android.support.v7.preference.ListPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import com.victorlapin.today.R
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.database.dao.CryptoCurrenciesDao
import org.koin.android.ext.android.inject

class CryptoCurrenciesSettingsFragment : PreferenceFragmentCompat() {
    private val mSettings by inject<SettingsManager>()
    private val mCryptoDao by inject<CryptoCurrenciesDao>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_crypto_currencies)

        val symbolPreference =
                findPreference(SettingsManager.KEY_CRYPTO_SYMBOL_TO) as ListPreference
        val value = mSettings.cryptoCurrenciesSymbolTo
        symbolPreference.value = value
        symbolPreference.summary = value
        symbolPreference.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { preference, newValue ->
                    preference.summary = newValue.toString()
                    mCryptoDao.clear()
                    true
                }

        val basesPreference =
                findPreference(SettingsManager.KEY_CRYPTO_SYMBOLS_FROM)
                        as MultiSelectListPreference
        val values = mSettings.cryptoCurrenciesSymbolsFrom
        basesPreference.values = values
        basesPreference.summary = mSettings.flatten(values)
        basesPreference.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { preference, newValue ->
                    preference.summary = mSettings.flatten(newValue.toString())
                    mCryptoDao.clear()
                    true
                }
    }
}