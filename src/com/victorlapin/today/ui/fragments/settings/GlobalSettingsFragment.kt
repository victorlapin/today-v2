package com.victorlapin.today.ui.fragments.settings

import android.os.Bundle
import android.support.v14.preference.SwitchPreference
import android.support.v7.preference.ListPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import com.victorlapin.today.R
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.manager.SyncManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.ui.activities.SettingsActivity
import org.koin.android.ext.android.inject

class GlobalSettingsFragment : PreferenceFragmentCompat() {
    private val mSettings by inject<SettingsManager>()
    private val mSyncManager by inject<SyncManager>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_global)

        val themePreference = findPreference(SettingsManager.KEY_THEME) as ListPreference
        val entries = arrayOf(
                getString(R.string.theme_light),
                getString(R.string.theme_dark),
                getString(R.string.theme_black))
        val values = arrayOf(
                Integer.toString(R.style.AppTheme_Light),
                Integer.toString(R.style.AppTheme_Dark),
                Integer.toString(R.style.AppTheme_Black))

        val value = mSettings.themeString

        themePreference.entries = entries
        themePreference.entryValues = values
        themePreference.value = value
        themePreference.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { _, newValue ->
                    (activity as SettingsActivity)
                            .onUpdateTheme(Integer.valueOf(newValue as String))
                    true
                }

        val jobPreference = findPreference(SettingsManager.KEY_USE_JOBSERVICE)
                as SwitchPreference
        jobPreference.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { _, newValue ->
                    if (newValue as Boolean) {
                        mSyncManager.scheduleJob()
                    } else {
                        mSyncManager.cancelJob()
                    }
                    return@OnPreferenceChangeListener true
                }
        val lastSync = mSettings.lastSync
        val jobSummary = context?.getString(R.string.pref_use_jobservice_last_sync)
        jobPreference.summary = if (lastSync > -1)
            jobSummary?.format(DateBuilder.formatter.format(lastSync))
        else jobSummary?.format(context?.getString(R.string.never))

        findPreference(SettingsManager.KEY_SET_AS_ASSISTANT).onPreferenceClickListener =
                Preference.OnPreferenceClickListener {
                    (activity as SettingsActivity).presenter
                            .openAssistantSystemSettings()
                    return@OnPreferenceClickListener true
                }

        // add a stub preference
//            if (Dimensions.hasSoftwareKeys(context!!)) {
//                val screen = findPreference(rootKey) as PreferenceScreen
//                val pref = Preference(context)
//                pref.isSelectable = false
//                screen.addPreference(pref)
//            }
    }
}