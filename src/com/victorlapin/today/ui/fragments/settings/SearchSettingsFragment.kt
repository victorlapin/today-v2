package com.victorlapin.today.ui.fragments.settings

import android.os.Bundle
import android.support.v7.preference.ListPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import com.victorlapin.today.R
import com.victorlapin.today.manager.SettingsManager
import org.koin.android.ext.android.inject

class SearchSettingsFragment : PreferenceFragmentCompat() {
    private val mSettings by inject<SettingsManager>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_search)

        val searchPreference =
                findPreference(SettingsManager.KEY_SEARCH_ENGINE) as ListPreference
        val entries = searchPreference.entries
        val values = arrayOfNulls<String>(entries.size)
        for (i in values.indices) {
            values[i] = Integer.toString(i)
        }
        searchPreference.entryValues = values
        val value = mSettings.searchEngine
        searchPreference.value = Integer.toString(value)
        searchPreference.summary = entries[value]
        searchPreference.onPreferenceChangeListener =
                Preference.OnPreferenceChangeListener { preference, newValue ->
                    val mValue = Integer.valueOf(newValue.toString())
                    preference.summary = entries[mValue]
                    true
                }
    }
}