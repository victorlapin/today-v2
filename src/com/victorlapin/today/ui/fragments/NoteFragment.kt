package com.victorlapin.today.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.manager.ServicesManager
import com.victorlapin.today.model.database.entity.Note
import com.victorlapin.today.presenter.NoteFragmentPresenter
import com.victorlapin.today.ui.activities.NoteActivity
import com.victorlapin.today.view.NoteFragmentView
import kotlinx.android.synthetic.main.fragment_note.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.release

class NoteFragment : BaseFragment(), NoteFragmentView {
    override val layoutRes = R.layout.fragment_note

    private val mPresenter by inject<NoteFragmentPresenter>()

    @InjectPresenter
    lateinit var presenter: NoteFragmentPresenter

    @ProvidePresenter
    fun providePresenter() = mPresenter

    private val mServices by inject<ServicesManager>()

    private var mNoteId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.showData(arguments?.getLong(NoteActivity.EXTRA_NOTE_ID))
    }

    override fun onStop() {
        super.onStop()
        release(Screens.FRAGMENT_NOTE)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) =
            inflater.inflate(R.menu.fragment_note, menu)

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_save -> {
            val text = edt_text.text.toString()
            if (text.isNotEmpty()) {
                mServices.inputManager.hideSoftInputFromWindow(edt_text.windowToken, 0)
                val note = Note(id = mNoteId, text = text)
                presenter.onSaveClicked(note)
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun setData(note: Note) {
        mNoteId = note.id
        edt_text.append(note.text)
    }

    companion object {
        fun newInstance(noteId: Long): Fragment {
            val fragment = NoteFragment()
            val args = Bundle()
            args.putLong(NoteActivity.EXTRA_NOTE_ID, noteId)
            fragment.arguments = args
            return fragment
        }
    }
}