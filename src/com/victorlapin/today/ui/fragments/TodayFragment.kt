package com.victorlapin.today.ui.fragments

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.addTo
import com.victorlapin.today.manager.AccountManager
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.EventType
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.presenter.TodayFragmentPresenter
import com.victorlapin.today.ui.adapters.TodayAdapter
import com.victorlapin.today.util.Dimensions
import com.victorlapin.today.view.TodayFragmentView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_today.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.release

class TodayFragment: BaseFragment(), TodayFragmentView,
        SwipeRefreshLayout.OnRefreshListener {
    override val layoutRes = R.layout.fragment_today

    private val mPresenter by inject<TodayFragmentPresenter>()

    @InjectPresenter
    lateinit var presenter: TodayFragmentPresenter

    @ProvidePresenter
    fun providePresenter() = mPresenter

    private val mAccounts by inject<AccountManager>()

    private val mAdapter by inject<TodayAdapter>()

    private val mAdapterItemClickEvent by inject<PublishSubject<BeanClickEventArgs>>("beanClick")

    private val mAdapterMenuClickEvent by inject<PublishSubject<MenuClickEventArgs>>("menuClick")

    private var mIsAssistantMode: Boolean = false

    private var mDisposable: CompositeDisposable? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mIsAssistantMode = activity!!.intent.action == Intent.ACTION_ASSIST
        list.apply {
            when (activity!!.resources.configuration.orientation) {
                Configuration.ORIENTATION_LANDSCAPE -> {
                    val lm = object : GridLayoutManager(context, GRID_COLUMN_COUNT) {
                        override fun getExtraLayoutSpace(state: RecyclerView.State?): Int {
                            return Dimensions.getScreenHeight(context!!)
                        }
                    }
                    lm.spanSizeLookup = object: GridLayoutManager.SpanSizeLookup() {
                        override fun getSpanSize(position: Int): Int {
                            return when (mAdapter.getItemViewType(position)) {
                                Bean.BEAN_TYPE_TOOLBAR,
                                Bean.BEAN_TYPE_FOOTER,
                                Bean.BEAN_TYPE_HEADER,
                                Bean.BEAN_TYPE_SEARCH,
                                Bean.BEAN_TYPE_SIGN_IN_GOOGLE,
                                Bean.BEAN_TYPE_GREETING -> GRID_COLUMN_COUNT
                                else -> 1
                            }
                        }
                    }
                    layoutManager = lm
                }
                else -> layoutManager = object : LinearLayoutManager(context) {
                    override fun getExtraLayoutSpace(state: RecyclerView.State?): Int {
                        return Dimensions.getScreenHeight(context!!)
                    }
                }
            }
            itemAnimator = DefaultItemAnimator()
            adapter = mAdapter
        }
        mDisposable = CompositeDisposable()
        mAdapterItemClickEvent
                .subscribe { presenter.onBeanClicked(it) }
                .addTo(mDisposable!!)
        mAdapterMenuClickEvent
                .subscribe { presenter.onMenuClicked(it) }
                .addTo(mDisposable!!)

        val typedValue = TypedValue()
        val theme = context!!.theme
        theme.resolveAttribute(R.attr.progress_background, typedValue, true)
        swipe_refresh.setProgressBackgroundColorSchemeColor(typedValue.data)
        swipe_refresh.setColorSchemeResources(R.color.accent)
        swipe_refresh.setOnRefreshListener(this)
    }

    override fun onDestroyView() {
        mDisposable?.clear()
        mDisposable = null
        super.onDestroyView()
    }

    override fun onStop() {
        super.onStop()
        release(Screens.FRAGMENT_TODAY)
    }

    override fun onRefresh() = presenter.reloadBeans()

    override fun showRefresh() {
        swipe_refresh.post { swipe_refresh.isRefreshing = true }
    }

    override fun hideRefresh() {
        swipe_refresh.post { swipe_refresh.isRefreshing = false }
    }

    override fun onClearBeans(vararg providers: String) {
        list.post { mAdapter.clear(*providers) }
    }

    override fun onNextBean(bean: Bean) {
        if (!(mIsAssistantMode && bean.excludeFromAssistant)) {
            list.post { mAdapter.add(bean) }
        }
    }

    override fun showListEmpty() {
        empty_view.post { empty_view.visibility = View.VISIBLE }
    }

    override fun hideListEmpty() {
        empty_view.post { empty_view.visibility = View.GONE }
    }

    override fun requestPermissions(permissions: Array<String>) =
            requestPermissions(permissions, REQUEST_PERMISSIONS_CODE)

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSIONS_CODE
                && grantResults.none { it == PackageManager.PERMISSION_DENIED })
        presenter.onRequestPermissionsResult()
    }

    override fun requestFitnessAccess() =
            GoogleSignIn.requestPermissions(this, REQUEST_OAUTH_CODE,
                    mAccounts.getGoogleAccount(), mAccounts.fitnessOptions)

    override fun signInWithGoogle() {
        val options = GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .build()
        val intent = GoogleSignIn.getClient(activity!!, options).signInIntent
        startActivityForResult(intent, REQUEST_SIGN_IN_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_OAUTH_CODE && resultCode == Activity.RESULT_OK) {
            presenter.onRequestPermissionsResult()
        } else if (requestCode == REQUEST_SIGN_IN_CODE && resultCode == Activity.RESULT_OK) {
            presenter.onSignInResult()
        }
    }

    override fun onProviderDisabled(provider: String) {
        // we need to get provider display name first
        val displayNames = context!!.resources.getStringArray(R.array.pref_providers_entries)
        val internalNames = context!!.resources.getStringArray(R.array.pref_providers_values)
        val index = internalNames.indexOf(provider)
        val displayName = displayNames[index]

        val text = context!!.getString(R.string.provider_disabled, displayName)
        val snackbar = Snackbar.make(coordinator, text, Snackbar.LENGTH_LONG)
        snackbar.setAction(R.string.action_undo) {
            val args = MenuClickEventArgs(EventType.ENABLE_PROVIDER, null, provider)
            presenter.onMenuClicked(args)
        }
        snackbar.setActionTextColor(ContextCompat.getColor(context!!, R.color.accent))
        snackbar.show()
    }

    companion object {
        private const val GRID_COLUMN_COUNT = 2
        private const val REQUEST_PERMISSIONS_CODE = 15
        private const val REQUEST_OAUTH_CODE = 16
        private const val REQUEST_SIGN_IN_CODE = 19

        fun newInstance() = TodayFragment()
    }
}