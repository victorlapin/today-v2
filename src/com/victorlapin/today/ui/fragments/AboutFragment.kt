package com.victorlapin.today.ui.fragments

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.model.AboutClickEventArgs
import com.victorlapin.today.model.repository.AboutRepository
import com.victorlapin.today.presenter.AboutFragmentPresenter
import com.victorlapin.today.ui.adapters.AboutAdapter
import com.victorlapin.today.view.AboutFragmentView
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.release

class AboutFragment : BaseFragment(), AboutFragmentView {
    override val layoutRes = R.layout.fragment_list

    private val mPresenter by inject<AboutFragmentPresenter>()

    @InjectPresenter
    lateinit var presenter: AboutFragmentPresenter

    @ProvidePresenter
    fun providePresenter() = mPresenter

    private val mAdapter by inject<AboutAdapter>()

    private val mClickEvent by inject<PublishSubject<AboutClickEventArgs>>("aboutClick")

    private var mDisposable: Disposable? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        list.apply {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
            adapter = mAdapter
        }
        mDisposable = mClickEvent.subscribe { presenter.onItemClick(it) }
    }

    override fun onDestroyView() {
        mDisposable?.dispose()
        mDisposable = null
        super.onDestroyView()
    }

    override fun onStop() {
        super.onStop()
        release(Screens.FRAGMENT_ABOUT)
    }

    override fun setData(data: List<AboutRepository.ListItem>) {
        list.post { mAdapter.setData(data) }
    }

    companion object {
        fun newInstance() = AboutFragment()
    }
}