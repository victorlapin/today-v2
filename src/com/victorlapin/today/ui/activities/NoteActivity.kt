package com.victorlapin.today.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.manager.ServicesManager
import com.victorlapin.today.presenter.NoteActivityPresenter
import com.victorlapin.today.ui.fragments.NoteFragment
import com.victorlapin.today.view.NoteActivityView
import kotlinx.android.synthetic.main.include_toolbar.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.release
import ru.terrakok.cicerone.android.SupportAppNavigator

class NoteActivity : BaseActivity(), NoteActivityView {
    override val layoutRes = R.layout.activity_generic

    private val mPresenter by inject<NoteActivityPresenter>()

    @InjectPresenter
    lateinit var presenter: NoteActivityPresenter

    @ProvidePresenter
    fun providePresenter() = mPresenter

    private val mServices by inject<ServicesManager>()

    private val mNoteId by lazy {
        intent.getLongExtra(EXTRA_NOTE_ID, -1)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adjustToolbar(toolbar)
        toolbar.setNavigationIcon(R.drawable.close)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        setTitle(if (mNoteId == (-1).toLong()) R.string.note_new else R.string.note_edit)
    }

    override fun onStop() {
        super.onStop()
        release(Screens.ACTIVITY_NOTE)
    }

    override fun onBackPressed() {
        mServices.inputManager.hideSoftInputFromWindow(toolbar.windowToken, 0)
        presenter.onBackPressed()
    }

    override val navigator = object : SupportAppNavigator(this, R.id.fragment_container) {
        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? = null

        override fun createFragment(screenKey: String?, data: Any?) =
                when (screenKey) {
                    Screens.FRAGMENT_NOTE -> NoteFragment.newInstance(mNoteId)
                    else -> null
                }
    }

    companion object {
        const val EXTRA_NOTE_ID = "EXTRA_NOTE_ID"
    }
}