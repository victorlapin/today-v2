package com.victorlapin.today.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.presenter.SettingsActivityPresenter
import com.victorlapin.today.ui.fragments.settings.*
import com.victorlapin.today.view.SettingsActivityView
import kotlinx.android.synthetic.main.include_toolbar.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.release
import ru.terrakok.cicerone.android.SupportAppNavigator

class SettingsActivity : BaseActivity(), SettingsActivityView {
    override val layoutRes = R.layout.activity_generic

    private val mPresenter by inject<SettingsActivityPresenter>()

    @InjectPresenter
    lateinit var presenter: SettingsActivityPresenter

    @ProvidePresenter
    fun providePresenter() = mPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adjustToolbar(toolbar)
        toolbar.setNavigationIcon(R.drawable.close)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }

        if (intent.hasExtra(EXTRA_SCREEN_KEY)) {
            presenter.showFragment(intent.getStringExtra(EXTRA_SCREEN_KEY))
        }
    }

    override fun onStop() {
        super.onStop()
        release(Screens.ACTIVITY_SETTINGS)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    fun onUpdateTheme(newTheme: Int) {
        updateTheme(newTheme)
    }

    override val navigator = object : SupportAppNavigator(this, R.id.fragment_container) {
        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? =
                when (screenKey) {
                    Screens.EXTERNAL_ASSISTANT ->
                        Intent(Settings.ACTION_VOICE_INPUT_SETTINGS)
                    else -> null
                }

        override fun createFragment(screenKey: String?, data: Any?) =
                when (screenKey) {
                    Screens.FRAGMENT_SETTINGS_GLOBAL -> GlobalSettingsFragment()
                    Screens.FRAGMENT_SETTINGS_WEATHER -> WeatherSettingsFragment()
                    Screens.FRAGMENT_SETTINGS_CURRENCIES -> CurrenciesSettingsFragment()
                    Screens.FRAGMENT_SETTINGS_CALLS -> CallsSettingsFragment()
                    Screens.FRAGMENT_SETTINGS_SEARCH -> SearchSettingsFragment()
                    Screens.FRAGMENT_SETTINGS_CRYPTO_CURRENCIES ->
                        CryptoCurrenciesSettingsFragment()
                    else -> null
                }
    }

    companion object {
        const val EXTRA_SCREEN_KEY = "EXTRA_SCREEN_KEY"
    }
}