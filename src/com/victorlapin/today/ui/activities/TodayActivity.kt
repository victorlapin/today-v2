package com.victorlapin.today.ui.activities

import android.app.SearchManager
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.provider.CalendarContract
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.victorlapin.today.R
import com.victorlapin.today.Screens
import com.victorlapin.today.model.bean.SearchBean
import com.victorlapin.today.presenter.TodayActivityPresenter
import com.victorlapin.today.ui.fragments.TodayFragment
import com.victorlapin.today.view.TodayActivityView
import org.koin.android.ext.android.inject
import org.koin.android.ext.android.release
import ru.terrakok.cicerone.android.SupportAppNavigator

class TodayActivity: BaseActivity(), TodayActivityView {
    override val layoutRes = R.layout.activity_today

    private val mPresenter by inject<TodayActivityPresenter>()

    @InjectPresenter
    lateinit var presenter: TodayActivityPresenter

    @ProvidePresenter
    fun providePresenter() = mPresenter

    override fun onStop() {
        super.onStop()
        release(Screens.ACTIVITY_TODAY)
    }

    override val navigator = object: SupportAppNavigator(this, R.id.fragment_container) {
        override fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? =
                when (screenKey) {
                    Screens.ACTIVITY_SETTINGS -> {
                        data?.let {
                            val intent = Intent(context, SettingsActivity::class.java)
                            intent.putExtra(SettingsActivity.EXTRA_SCREEN_KEY, it.toString())
                            return intent
                        }
                        null
                    }
                    Screens.ACTIVITY_ABOUT -> {
                        Intent(context, AboutActivity::class.java)
                    }
                    Screens.ACTIVITY_NOTE -> {
                        val intent = Intent(context, NoteActivity::class.java)
                        data?.let {
                            intent.putExtra(NoteActivity.EXTRA_NOTE_ID, it as Long)
                        }
                        intent
                    }
                    Screens.EXTERNAL_CALENDAR -> {
                        data?.let {
                            val eventId = (it as Long)
                            val eventIntent = Intent(Intent.ACTION_VIEW)
                            eventIntent.data = ContentUris
                                    .withAppendedId(CalendarContract.Events.CONTENT_URI, eventId)
                            eventIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            val ri = packageManager.resolveActivity(eventIntent, 0)
                            ri?.let {
                                return eventIntent
                            }
                        }
                        null
                    }
                    Screens.EXTERNAL_CONTACT -> {
                        data?.let {
                            val uri = (it as Uri)
                            val contactIntent = Intent(Intent.ACTION_VIEW)
                            contactIntent.data = uri
                            contactIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            val ri = packageManager.resolveActivity(contactIntent, 0)
                            ri?.let {
                                return contactIntent
                            }
                        }
                        null
                    }
                    Screens.EXTERNAL_BLUETOOTH -> {
                        val btIntent = Intent()
                        btIntent.action = android.provider.Settings.ACTION_BLUETOOTH_SETTINGS
                        btIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        btIntent
                    }
                    Screens.EXTERNAL_SEARCH -> {
                        data?.let {
                            val bundle = it as Bundle
                            val query = bundle.getString(SearchBean.EXTRA_QUERY)
                            val appPackage = bundle.getString(SearchBean.EXTRA_APP_PACKAGE)
                            val url = bundle.getString(SearchBean.EXTRA_URL)

                            var searchIntent: Intent?
                            var info: ResolveInfo?

                            // try to launch the app first
                            searchIntent = Intent(Intent.ACTION_WEB_SEARCH)
                            searchIntent.putExtra(SearchManager.QUERY, query)
                            searchIntent.`package` = appPackage
                            info = packageManager.resolveActivity(searchIntent, 0)
                            if (info == null) {
                                // fallback to web search
                                searchIntent = Intent(Intent.ACTION_VIEW)
                                searchIntent.data = Uri.parse(String.format(url, query))
                                info = packageManager.resolveActivity(searchIntent, 0)
                                if (info == null) {
                                    // ignore
                                    searchIntent = null
                                }
                            }
                            return searchIntent
                        }
                    }
                    else -> null
                }

        override fun createFragment(screenKey: String?, data: Any?) =
                when (screenKey) {
                    Screens.FRAGMENT_TODAY -> TodayFragment.newInstance()
                    else -> null
                }
    }
}