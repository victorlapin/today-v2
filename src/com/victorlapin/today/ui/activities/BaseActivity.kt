package com.victorlapin.today.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.annotation.StyleRes
import android.support.design.widget.AppBarLayout
import android.support.v7.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatActivity
import com.victorlapin.today.R
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.util.Dimensions
import kotlinx.android.synthetic.main.include_toolbar.*
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportAppNavigator

abstract class BaseActivity: MvpAppCompatActivity() {
    abstract val layoutRes: Int
    abstract val navigator: SupportAppNavigator?

    private val mNavigationHolder by inject<NavigatorHolder>()
    private val mSettings by inject<SettingsManager>()

    @StyleRes private var mCurrentTheme: Int = 0
    private val mHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        val action = this.intent.action
        mCurrentTheme = mSettings.theme
        if (action != null && action == Intent.ACTION_ASSIST) {
            val assistantTheme = if (mCurrentTheme == R.style.AppTheme_Light)
                R.style.AppTheme_Assistant_Light else R.style.AppTheme_Assistant_Dark
            setTheme(assistantTheme)
        } else {
            setTheme(mCurrentTheme)
        }
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
        setSupportActionBar(toolbar)
    }

    override fun onResume() {
        super.onResume()

        val newTheme = mSettings.theme
        if (mCurrentTheme != R.style.AppTheme_Assistant_Light
                && mCurrentTheme != R.style.AppTheme_Assistant_Dark
                && mCurrentTheme != newTheme) {
            mHandler.post { updateTheme(newTheme) }
        }
    }

    protected fun updateTheme(@StyleRes newTheme: Int) {
        mCurrentTheme = newTheme
        recreate()
    }

    protected fun adjustToolbar(toolbar: Toolbar) {
        val params = toolbar.layoutParams as AppBarLayout.LayoutParams
        params.topMargin += Dimensions.statusBarHeight
        toolbar.layoutParams = params
    }

    override fun onPause() {
        mNavigationHolder.removeNavigator()
        super.onPause()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        mNavigationHolder.setNavigator(navigator)
    }
}