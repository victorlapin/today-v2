package com.victorlapin.today

import android.app.Application
import com.victorlapin.today.di.allModules
import org.koin.android.ext.android.startKoin
import org.koin.android.logger.AndroidLogger
import org.koin.log.EmptyLogger

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
        val logger = if (BuildConfig.DEBUG) AndroidLogger() else EmptyLogger()
        startKoin(context = this, modules = allModules, logger = logger)
    }

    companion object {
        lateinit var instance: App
            private set
    }
}