package com.victorlapin.today.di

import com.victorlapin.today.Screens
import com.victorlapin.today.model.AboutClickEventArgs
import com.victorlapin.today.model.BeanClickEventArgs
import com.victorlapin.today.model.MenuClickEventArgs
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.presenter.*
import com.victorlapin.today.ui.adapters.AboutAdapter
import com.victorlapin.today.ui.adapters.BeanDelegateAdapter
import com.victorlapin.today.ui.adapters.TodayAdapter
import com.victorlapin.today.ui.adapters.delegates.*
import io.reactivex.subjects.PublishSubject
import org.koin.dsl.module.module

val activitiesModule = module {
    module(Screens.ACTIVITY_TODAY) {
        factory { TodayActivityPresenter(get()) }
        factory { AssistantActivityPresenter(get()) }
    }

    module(Screens.FRAGMENT_TODAY) {
        factory { TodayFragmentPresenter(get(), get(), get(), get()) }
        factory { TodayAdapter(get("delegates"), get()) }
        single("beanClick") { PublishSubject.create<BeanClickEventArgs>() }
        single("menuClick") { PublishSubject.create<MenuClickEventArgs>() }
        single("delegates") {
            val result = HashMap<Int, BeanDelegateAdapter>()
            result[Bean.BEAN_TYPE_CALENDAR] = CalendarDelegateAdapter(get("beanClick"),
                    get("menuClick"))
            result[Bean.BEAN_TYPE_CALLS] = CallsDelegateAdapter(get("beanClick"),
                    get("menuClick"), get())
            result[Bean.BEAN_TYPE_CURRENCIES] = CurrenciesDelegateAdapter(get("menuClick"))
            result[Bean.BEAN_TYPE_FOOTER] = FooterDelegateAdapter()
            result[Bean.BEAN_TYPE_SIGN_IN_GOOGLE] =
                    GoogleSignInDelegateAdapter(get("beanClick"))
            result[Bean.BEAN_TYPE_HEADER] = HeaderDelegateAdapter()
            result[Bean.BEAN_TYPE_PERMISSIONS_OAUTH] = OAuthDelegateAdapter(get("beanClick"))
            result[Bean.BEAN_TYPE_PERMISSIONS] = PermissionsDelegateAdapter(get("beanClick"))
            result[Bean.BEAN_TYPE_STEPS] = StepsDelegateAdapter(get("menuClick"))
            result[Bean.BEAN_TYPE_TOOLBAR] = ToolbarDelegateAdapter(get("menuClick"))
            result[Bean.BEAN_TYPE_WEARABLES] = WearablesDelegateAdapter(get("beanClick"),
                    get("menuClick"))
            result[Bean.BEAN_TYPE_WEATHER] = WeatherDelegateAdapter(get(), get("menuClick"))
            result[Bean.BEAN_TYPE_SEARCH] = SearchDelegateAdapter(get("beanClick"),
                    get("menuClick"))
            result[Bean.BEAN_TYPE_CRYPTO_CURRENCIES] =
                    CryptoCurrenciesDelegateAdapter(get("menuClick"), get())
            result[Bean.BEAN_TYPE_NOTES] =
                    NotesDelegateAdapter(get("menuClick"))
            result[Bean.BEAN_TYPE_GREETING] = GreetingDelegateAdapter()
            result[Bean.BEAN_TYPE_NOTES_PLACEHOLDER] =
                    NotePlaceholderDelegateAdapter(get("beanClick"), get("menuClick"))
            return@single result
        }
    }

    module(Screens.ACTIVITY_SETTINGS) {
        factory { SettingsActivityPresenter(get()) }
    }

    module(Screens.ACTIVITY_ABOUT) {
        factory { AboutActivityPresenter(get()) }
    }

    module(Screens.FRAGMENT_ABOUT) {
        factory { AboutFragmentPresenter(get(), get()) }
        factory { AboutAdapter(get(), get("aboutClick")) }
        single("aboutClick") { PublishSubject.create<AboutClickEventArgs>() }
    }

    module(Screens.ACTIVITY_NOTE) {
        factory { NoteActivityPresenter(get()) }
    }

    module(Screens.FRAGMENT_NOTE) {
        factory { NoteFragmentPresenter(get(), get()) }
    }
}