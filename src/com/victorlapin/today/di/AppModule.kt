package com.victorlapin.today.di

import com.victorlapin.today.manager.*
import org.koin.dsl.module.module
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

val appModule = module {
    single { Cicerone.create() as Cicerone<Router> }
    single { get<Cicerone<Router>>().router }
    single { get<Cicerone<Router>>().navigatorHolder }

    single { AccountManager(get()) }
    single { HttpManager(get()) }
    single { ResourcesManager(get()) }
    single { ServicesManager(get()) }
    single { SettingsManager(get(), get()) }
    single { SyncManager() }
}