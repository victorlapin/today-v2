package com.victorlapin.today.di

val allModules = listOf(
        appModule,
        modelModule,
        activitiesModule
)