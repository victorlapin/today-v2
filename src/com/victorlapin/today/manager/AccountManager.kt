package com.victorlapin.today.manager

import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension
import com.google.android.gms.awareness.Awareness
import com.google.android.gms.awareness.SnapshotClient
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.HistoryClient
import com.google.android.gms.fitness.data.DataType

class AccountManager(private val mContext: Context) {
    fun isPlayServicesAvailable(): Boolean {
        val result = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(mContext)
        return when (result) {
            ConnectionResult.SERVICE_MISSING,
            ConnectionResult.SERVICE_DISABLED,
            ConnectionResult.SERVICE_INVALID -> false
            else -> true
        }
    }

    fun getGoogleAccount(): GoogleSignInAccount? {
        return GoogleSignIn.getLastSignedInAccount(mContext)
    }

    fun isGoogleSignedIn() = getGoogleAccount() != null

    fun getGoogleAccountName(): String? =
            getGoogleAccount()?.displayName

    fun hasPermission(permission: String) =
            ContextCompat.checkSelfPermission(mContext, permission) ==
                    PackageManager.PERMISSION_GRANTED

    fun hasGooglePermission(options: GoogleSignInOptionsExtension) =
            isGoogleSignedIn() && GoogleSignIn.hasPermissions(getGoogleAccount(), options)

    fun getFitnessHistoryClient(): HistoryClient? = if (isGoogleSignedIn())
        Fitness.getHistoryClient(mContext, getGoogleAccount()!!) else null

    val fitnessOptions: GoogleSignInOptionsExtension = FitnessOptions.builder()
            .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
            .addDataType(DataType.TYPE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
            .build()

    fun getAwarenessSnapshotClient(): SnapshotClient? = Awareness.getSnapshotClient(mContext)
}