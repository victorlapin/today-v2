package com.victorlapin.today.manager

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.preference.PreferenceManager
import com.victorlapin.today.R
import com.victorlapin.today.model.bean.SearchBean

class SettingsManager(
        context: Context,
        private val mResources: ResourcesManager
) {
    companion object {
        const val KEY_THEME = "interface_theme"
        const val KEY_CURRENCIES_BASES = "currencies_bases"
        const val KEY_CURRENCIES_SYMBOL = "currencies_symbol"
        const val KEY_TIMEOUT = "timeout"
        const val KEY_CALLS_COUNT = "calls_count"
        const val KEY_CALLS_TYPES = "calls_types"
        const val KEY_WEATHER_UNITS = "weather_units"
        const val KEY_WEATHER_SHOW_FORECAST = "weather_show_forecast"
        const val KEY_FEED_PROVIDERS = "feed_providers"
        const val KEY_SEARCH_ENGINE = "search_engine"
        const val KEY_USE_JOBSERVICE = "use_jobservice"
        const val KEY_JOBSERVICE_LAST_SYNC = "jobservice_last_sync"
        const val KEY_SET_AS_ASSISTANT = "set_as_assistant"
        const val KEY_CRYPTO_SYMBOLS_FROM = "crypto_currencies_symbols_from"
        const val KEY_CRYPTO_SYMBOL_TO = "crypto_currencies_symbol_to"
    }

    private val mPrefs = PreferenceManager.getDefaultSharedPreferences(context)

    fun bindListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        mPrefs.registerOnSharedPreferenceChangeListener(listener)
    }

    fun unbindListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        mPrefs.unregisterOnSharedPreferenceChangeListener(listener)
    }

    fun flatten(set: Set<String>) = flatten(set.toString())
    fun flatten(set: String) = set.replace("\\[|\\]|\\{|\\}".toRegex(), "")

    val themeString: String
        get() = mPrefs.getString(KEY_THEME, R.style.AppTheme_Light.toString())

    val theme: Int
        get() = Integer.parseInt(themeString)

    val currenciesBases: Set<String>
        get() = mPrefs.getStringSet(KEY_CURRENCIES_BASES,
                mResources.getStringSet(R.array.pref_currencies_default))

    val currenciesSymbol: String
        get() = mPrefs.getString(KEY_CURRENCIES_SYMBOL, "RUB")

    var timeout: Int
        get() = mPrefs.getInt(KEY_TIMEOUT, 5)
        set(timeout) = mPrefs.edit().putInt(KEY_TIMEOUT, timeout).apply()

    private var providers: Set<String>
        get() = mPrefs.getStringSet(KEY_FEED_PROVIDERS,
                mResources.getStringSet(R.array.pref_providers_values))
        set(set) = mPrefs.edit().putStringSet(KEY_FEED_PROVIDERS, set).apply()

    fun isProviderEnabled(provider: String): Boolean {
        return providers.contains(provider)
    }

    fun disableProvider(provider: String?): Boolean {
        provider?.let {
            if (isProviderEnabled(it)) {
                providers = providers.minusElement(it)
                return true
            }
        }
        return false
    }

    fun enableProvider(provider: String?): Boolean {
        provider?.let {
            if (!isProviderEnabled(it)) {
                providers = providers.plusElement(it)
                return true
            }
        }
        return false
    }

    val callsCount: Int
        get() = mPrefs.getInt(KEY_CALLS_COUNT, 3)

    val callsTypes: Set<String>
        get() = mPrefs.getStringSet(KEY_CALLS_TYPES,
                mResources.getStringSet(R.array.pref_calls_types_values))

    val weatherUnits: String
        get() = mPrefs.getString(KEY_WEATHER_UNITS, "C")

    val isWeatherForecastEnabled: Boolean
        get() = mPrefs.getBoolean(KEY_WEATHER_SHOW_FORECAST, true)

    val searchEngine: Int
        get() {
            val value = mPrefs.getString(KEY_SEARCH_ENGINE,
                    Integer.toString(SearchBean.DUCKDUCKGO))
            return Integer.valueOf(value)
        }

    var lastSync: Long
        get() = mPrefs.getLong(KEY_JOBSERVICE_LAST_SYNC, -1)
        set(value) = mPrefs.edit().putLong(KEY_JOBSERVICE_LAST_SYNC, value).apply()

    val cryptoCurrenciesSymbolsFrom: Set<String>
        get() = mPrefs.getStringSet(KEY_CRYPTO_SYMBOLS_FROM,
                mResources.getStringSet(R.array.pref_crypto_currencies_default))

    val cryptoCurrenciesSymbolTo: String
        get() = mPrefs.getString(KEY_CRYPTO_SYMBOL_TO, "USD")
}