package com.victorlapin.today.manager

import android.content.Context
import android.location.Geocoder
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Looper
import android.view.inputmethod.InputMethodManager
import java.util.*

class ServicesManager(private val mContext: Context) {
    private val connectivityManager by lazy {
        mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    val locationManager by lazy {
        mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    val geocoder by lazy {
        Geocoder(mContext, Locale.getDefault())
    }

    fun isConnectedToInternet(): Boolean {
        return connectivityManager.activeNetworkInfo != null
                && connectivityManager.activeNetworkInfo.isConnected
    }

    val looper: Looper by lazy {
        mContext.mainLooper
    }

    val inputManager by lazy {
        mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }
}