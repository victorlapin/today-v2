package com.victorlapin.today.manager

import androidx.work.WorkManager
import com.victorlapin.today.sync.DataFetchWorker

class SyncManager {
    fun scheduleJob() =
            WorkManager.getInstance()?.enqueue(DataFetchWorker.buildRequest())

    fun cancelJob() =
            WorkManager.getInstance()?.cancelAllWorkByTag(DataFetchWorker.JOB_TAG)
}