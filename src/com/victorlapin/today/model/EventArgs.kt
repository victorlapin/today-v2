package com.victorlapin.today.model

data class BeanClickEventArgs(val beanType: Int, val data: Any?)

data class MenuClickEventArgs(val eventType: EventType, val screenKey: String?, val data: Any?)

data class AboutClickEventArgs(val screenKey: String, val data: Any?)

enum class EventType {
    NAVIGATE, DISABLE_PROVIDER, ENABLE_PROVIDER, EDIT_NOTE, DELETE_NOTE
}