package com.victorlapin.today.model.interactor

import com.victorlapin.today.model.database.entity.Note
import com.victorlapin.today.model.repository.NotesBeansRepository

class NotesInteractor(
        private val mNotesRepo: NotesBeansRepository
) {
    fun updateNote(note: Note) = mNotesRepo.updateNote(note)

    fun deleteNote(noteId: Long) = mNotesRepo.deleteNote(noteId)

    fun addNote(note: Note) = mNotesRepo.addNote(note)

    fun getNoteById(id: Long) = mNotesRepo.getNoteById(id)
}