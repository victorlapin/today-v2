package com.victorlapin.today.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.victorlapin.today.model.database.dao.CryptoCurrenciesDao
import com.victorlapin.today.model.database.dao.CurrenciesDao
import com.victorlapin.today.model.database.dao.NotesDao
import com.victorlapin.today.model.database.dao.WeatherDao
import com.victorlapin.today.model.database.entity.*

@Database(entities = [
    Currency::class,
    CurrencyMeta::class,
    CryptoCurrency::class,
    CryptoCurrencyMeta::class,
    WeatherForecast::class,
    TodayWeather::class,
    Note::class
], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getCurrenciesDao(): CurrenciesDao
    abstract fun getCryptoCurrenciesDao(): CryptoCurrenciesDao
    abstract fun getWeatherDao(): WeatherDao
    abstract fun getNotesDao(): NotesDao
}