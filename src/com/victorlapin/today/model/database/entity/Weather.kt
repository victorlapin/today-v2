package com.victorlapin.today.model.database.entity

import android.arch.persistence.room.*

@Entity(tableName = "weather_forecast",
        foreignKeys = [(ForeignKey(entity = TodayWeather::class,
                parentColumns = ["id"], childColumns = ["weather_id"],
                onDelete = ForeignKey.CASCADE))],
        indices = [(Index(name = "idx_weather", value = ["weather_id"]))])
data class WeatherForecast(
        @PrimaryKey(autoGenerate = true)
        val id: Long? = null,
        @ColumnInfo(name = "weather_id")
        val weatherId: Long,
        @ColumnInfo(name = "temp_c")
        val tempC: Double,
        @ColumnInfo(name = "temp_f")
        val tempF: Double,
        val condition: String,
        val date: String
)

@Entity(tableName = "weather")
data class TodayWeather(
        @PrimaryKey(autoGenerate = true)
        val id: Long? = null,
        @ColumnInfo(name = "last_update_dt")
        val lastUpdateDT: Long,
        val provider: String,
        val city: String,
        @ColumnInfo(name = "temp_c")
        val tempC: Double,
        @ColumnInfo(name = "temp_f")
        val tempF: Double,
        @ColumnInfo(name = "temp_like_c")
        val tempLikeC: Double?,
        @ColumnInfo(name = "temp_like_f")
        val tempLikeF: Double?,
        val humidity: Int,
        val condition: String?,
        val condition1: Int?,
        val condition2: Int?
)

class WeatherWithForecast {
    @Embedded
    var weather: TodayWeather? = null

    @Relation(parentColumn = "id", entity = WeatherForecast::class, entityColumn = "weather_id")
    var forecast: List<WeatherForecast> = listOf()
}