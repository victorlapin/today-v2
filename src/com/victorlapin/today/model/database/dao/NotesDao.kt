package com.victorlapin.today.model.database.dao

import android.arch.persistence.room.*
import com.victorlapin.today.model.database.entity.Note

@Dao
interface NotesDao {
    @Query("select * from notes limit 50")
    fun getNotes(): List<Note>

    @Query("select * from notes where id = :id")
    fun getNoteById(id: Long): Note

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(note: Note)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(note: Note)

    @Query("delete from notes where id = :id")
    fun delete(id: Long)
}