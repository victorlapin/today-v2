package com.victorlapin.today.model.database.dao

import android.arch.persistence.room.*
import com.victorlapin.today.model.database.entity.CurrenciesWithMeta
import com.victorlapin.today.model.database.entity.Currency
import com.victorlapin.today.model.database.entity.CurrencyMeta

@Dao
abstract class CurrenciesDao {
    @Transaction
    @Query("select * from currencies_meta limit 1")
    abstract fun getCurrencies(): CurrenciesWithMeta?

    @Transaction
    open fun insert(data: CurrenciesWithMeta) {
        data.meta?.let { clear(); insert(it) }
        data.currencies.forEach { insert(it) }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(data: Currency)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(data: CurrencyMeta)

    @Query("delete from currencies_meta")
    abstract fun clear()
}