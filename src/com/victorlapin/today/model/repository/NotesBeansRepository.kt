package com.victorlapin.today.model.repository

import com.victorlapin.today.R
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.NoteBean
import com.victorlapin.today.model.bean.NotePlaceholderBean
import com.victorlapin.today.model.database.dao.NotesDao
import com.victorlapin.today.model.database.entity.Note
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class NotesBeansRepository(
        private val mResources: ResourcesManager,
        private val mSettings: SettingsManager,
        private val mNotesDao: NotesDao
) : BeansRepository {
    override val name = "notes"

    override fun provideBeans(dateBuilder: DateBuilder): Observable<Bean> {
        if (mSettings.isProviderEnabled(name)) {
            return Observable.create<Bean> { emitter ->
                val header = mResources.getString(R.string.provider_notes)
                val placeholderBean = NotePlaceholderBean(name, NoteBean.ID_SEQUENCE, header)
                placeholderBean.text = mResources.getString(R.string.tap_to_add)
                emitter.onNext(placeholderBean)

                var index = 1
                mNotesDao.getNotes()
                        .forEach {
                            val bean = NoteBean(name, NoteBean.ID_SEQUENCE + index, header)
                            bean.noteId = it.id
                            bean.text = it.text
                            index++
                            emitter.onNext(bean)
                        }
                emitter.onComplete()
            }
                    .subscribeOn(Schedulers.io())
        }
        return Observable.empty()
    }

    override fun isCacheValid(dateBuilder: DateBuilder) = true

    fun updateNote(note: Note) = mNotesDao.update(note)

    fun deleteNote(noteId: Long) = mNotesDao.delete(noteId)

    fun addNote(note: Note) = mNotesDao.insert(note)

    fun getNoteById(id: Long) = mNotesDao.getNoteById(id)
}