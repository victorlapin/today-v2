package com.victorlapin.today.model.repository

import com.victorlapin.today.R
import com.victorlapin.today.manager.HttpManager
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.manager.ServicesManager
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.CryptoCurrenciesBean
import com.victorlapin.today.model.database.dao.CryptoCurrenciesDao
import com.victorlapin.today.model.database.entity.CryptoCurrenciesWithMeta
import com.victorlapin.today.model.database.entity.CryptoCurrency
import com.victorlapin.today.model.database.entity.CryptoCurrencyMeta
import io.reactivex.Observable
import org.json.JSONObject
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

class CryptoCurrenciesBeansRepository(
        private val mSettings: SettingsManager,
        private val mHttpRepo: HttpManager,
        private val mResources: ResourcesManager,
        private val mServices: ServicesManager,
        private val mCryptoDao: CryptoCurrenciesDao
) : BeansRepository {
    interface CryptoCompareApi {
        @GET("pricemulti")
        fun getPriceMulti(@Query("fsyms") fromSymbols: String,
                          @Query("tsyms") toSymbols: String): Observable<String>
    }

    private val mCryptoCompareUrl = "https://min-api.cryptocompare.com/data/"
    private val mDelta: Long = TimeUnit.HOURS.toMillis(3)
    private var mLastCrypto: CryptoCurrenciesWithMeta? = null

    private val mImageRes by lazy {
        val result = HashMap<String, Int>()
        result["BTC"] = R.drawable.btc_logo
        result["ETH"] = R.drawable.eth_logo
        result["LTC"] = R.drawable.litecoin_logo
        result["XMR"] = R.drawable.xmr_logo
        return@lazy result
    }

    override val name = "crypto_currencies"

    override fun provideBeans(dateBuilder: DateBuilder): Observable<Bean> {
        if (mSettings.isProviderEnabled(name)) {
            mLastCrypto = mCryptoDao.getCryptoCurrencies()
            if (isCacheValid(dateBuilder)) {
                return getCachedData()
                        .map { createBean(it) }
            }
            if (mServices.isConnectedToInternet()) {
                return loadData(dateBuilder)
                        .map { createBean(it) }
            }
        }
        return Observable.empty()
    }

    override fun isCacheValid(dateBuilder: DateBuilder): Boolean {
        return mLastCrypto != null && mLastCrypto?.meta!!.lastUpdateDT > 0
                && dateBuilder.now - mLastCrypto?.meta!!.lastUpdateDT <= mDelta
    }

    private fun loadData(dateBuilder: DateBuilder): Observable<CryptoCurrenciesWithMeta> {
        val fSym = mSettings.flatten(mSettings.cryptoCurrenciesSymbolsFrom)
                .replace(" ", "")
        val tSym = mSettings.cryptoCurrenciesSymbolTo

        try {
            val api = mHttpRepo.getRetrofit(mCryptoCompareUrl).create(CryptoCompareApi::class.java)
            return api.getPriceMulti(fSym, tSym)
                    .map {
                        val json = JSONObject(it)
                        val curs = ArrayList<CryptoCurrency>()
                        json.keys()
                                .forEach {
                                    val coinRate = json.getJSONObject(it)
                                    val symbol = coinRate.names().get(0).toString()
                                    val amount = coinRate.getDouble(symbol)
                                    //TODO: get rid of hardcoded foreign key ids
                                    curs.add(CryptoCurrency(
                                            metaId = 1,
                                            key = it,
                                            data = "1 $it = ${"%.2f".format(amount)} $symbol"))
                                }

                        val result = CryptoCurrenciesWithMeta()
                        result.meta = CryptoCurrencyMeta(id = 1, lastUpdateDT = dateBuilder.now)
                        result.currencies = curs
                        mLastCrypto = result
                        mCryptoDao.insert(result)
                        return@map result
                    }
                    .onErrorResumeNext { t: Throwable ->
                        t.printStackTrace()
                        Observable.empty()
                    }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return Observable.empty()
        }
    }

    private fun getCachedData(): Observable<CryptoCurrenciesWithMeta> =
            Observable.just(mLastCrypto)

    private fun createBean(data: CryptoCurrenciesWithMeta): Bean {
        val bean = CryptoCurrenciesBean(name,
                mResources.getString(R.string.provider_crypto_currencies))
        bean.source = "Cryptocompare.com"
        bean.lastUpdateTime = DateBuilder.formatter.format(Date(data.meta!!.lastUpdateDT))
        data.currencies.forEach {
            bean.items.add(CryptoCurrenciesBean.Item(it.data, mImageRes[it.key]))
        }
        return bean
    }
}