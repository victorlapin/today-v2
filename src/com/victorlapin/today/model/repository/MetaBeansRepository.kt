package com.victorlapin.today.model.repository

import android.support.annotation.StringRes
import com.victorlapin.today.R
import com.victorlapin.today.manager.AccountManager
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.*
import io.reactivex.Observable
import java.security.InvalidParameterException

class MetaBeansRepository(
        private val mResources: ResourcesManager,
        private val mAccounts: AccountManager,
        private val mSettings: SettingsManager
) : BeansRepository {
    override val name = "meta"

    override fun provideBeans(dateBuilder: DateBuilder): Observable<Bean> {
        val beans = ArrayList<Bean>()
        beans.add(HeaderBean())
        beans.add(FooterBean())

        @StringRes val searchEngineRes: Int
        val searchUrl: String
        val searchApp: String
        when (mSettings.searchEngine) {
            SearchBean.GOOGLE -> {
                searchEngineRes = R.string.search_engine_google
                searchUrl = "https://www.google.com/search?q=%s"
                searchApp = "com.google.android.googlequicksearchbox"
            }
            SearchBean.DUCKDUCKGO -> {
                searchEngineRes = R.string.search_engine_duckduckgo
                searchUrl = "https://duckduckgo.com/?q=%s"
                searchApp = "com.duckduckgo.mobile.android"
            }
            SearchBean.BING -> {
                searchEngineRes = R.string.search_engine_bing
                searchUrl = "https://www.bing.com/search?q=%s"
                searchApp = "com.microsoft.bing"
            }
            SearchBean.YANDEX -> {
                searchEngineRes = R.string.search_engine_yandex
                searchUrl = "https://www.yandex.ru/search/?text=%s"
                searchApp = "ru.yandex.searchplugin"
            }
            else -> throw InvalidParameterException()
        }
        val searchHint = mResources.getString(R.string.search_hint)
                .format(mResources.getString(searchEngineRes))
        beans.add(SearchBean(searchHint, searchUrl, searchApp))
        beans.add(ToolbarBean())

        val appName = mResources.getString(R.string.app_name)
        if (mAccounts.isGoogleSignedIn()) {
            beans.add(GreetingBean(appName, mAccounts.getGoogleAccountName()))
        } else {
            beans.add(GreetingBean(appName, null))
            if (mAccounts.isPlayServicesAvailable()) {
                beans.add(GoogleSignInBean())
            }
        }

        return Observable.fromIterable(beans)
    }

    override fun isCacheValid(dateBuilder: DateBuilder) = true
}