package com.victorlapin.today.model.repository

import android.Manifest
import android.annotation.SuppressLint
import android.provider.CalendarContract
import com.android.calendarcommon2.Duration
import com.victorlapin.today.R
import com.victorlapin.today.manager.AccountManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.CalendarBean
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.manager.SettingsManager
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class CalendarBeansRepository(
        private val mSettings: SettingsManager,
        private val mResources: ResourcesManager,
        private val mAccounts: AccountManager
) : BeansRepository {
    private fun hasPermission() = mAccounts.hasPermission(Manifest.permission.READ_CALENDAR)
    private val mDateFormatter =
            SimpleDateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.SHORT)
    private val mTimeFormatter =
            SimpleDateFormat.getTimeInstance(DateFormat.SHORT)
    private val mInstancesProjection = arrayOf(
            CalendarContract.Instances._ID, // 0
            CalendarContract.Instances.EVENT_ID, // 1
            CalendarContract.Instances.BEGIN, // 2
            CalendarContract.Instances.END, // 3
            CalendarContract.Instances.DURATION, // 4
            CalendarContract.Instances.ALL_DAY) // 5
    private val mEventsProjection = arrayOf(
            CalendarContract.Events._ID, // 0
            CalendarContract.Events.TITLE, // 1
            CalendarContract.Events.DESCRIPTION, // 2
            CalendarContract.Events.EVENT_LOCATION, // 3
            CalendarContract.Events.CALENDAR_DISPLAY_NAME, // 4
            CalendarContract.Events.CALENDAR_COLOR) // 5
    private val mCalendarStart = Calendar.getInstance()
    private val mCalendarEnd = Calendar.getInstance()
    private val mDuration = Duration()

    override val name = "calendar"

    @SuppressLint("MissingPermission")
    override fun provideBeans(dateBuilder: DateBuilder): Observable<Bean> {
        if (mSettings.isProviderEnabled(name)) {
            if (hasPermission()) {
                return Observable.create<Bean> { emitter ->
                    var index = 0
                    val instancesCursor = CalendarContract.Instances.query(
                            mResources.resolver,
                            mInstancesProjection, dateBuilder.now, dateBuilder.tomorrow)
                    instancesCursor.use { cursor ->
                        while (cursor.moveToNext()) {
                            val eventId = cursor.getLong(1)
                            var dtStart = cursor.getLong(2)
                            var dtEnd = cursor.getLong(3)
                            val strDuration = cursor.getString(4)
                            val isAllDay = cursor.getInt(5) == 1

                            val eventsCursor = mResources.resolver.query(
                                    CalendarContract.Events.CONTENT_URI,
                                    mEventsProjection,
                                    CalendarContract.Events._ID + " =  ?",
                                    arrayOf(eventId.toString()), null)
                            if (eventsCursor != null) {
                                eventsCursor.moveToFirst()

                                val bean = CalendarBean(name,CalendarBean.ID_SEQUENCE + index,
                                        mResources.getString(R.string.provider_calendar))
                                bean.eventId = eventId
                                bean.isAllDayEvent = isAllDay
                                if (isAllDay) {
                                    // explicitly set time span 00:00 - 23:59 for all day events
                                    val db = DateBuilder()
                                    dtStart = db.today
                                    dtEnd = db.tomorrow
                                    bean.dateStart = mTimeFormatter.format(dtStart)
                                    bean.dateEnd = mTimeFormatter.format(dtEnd)
                                } else {
                                    if (dtEnd <= 0) {
                                        // calculate end date by duration
                                        try {
                                            mDuration.parse(strDuration)
                                            dtEnd = dtStart + mDuration.millis
                                        } catch (ex: Exception) {
                                            ex.printStackTrace()
                                        }

                                    }
                                    // now calculate whether it is the same day
                                    mCalendarStart.timeInMillis = dtStart
                                    mCalendarEnd.timeInMillis = dtEnd
                                    val isSameDay = mCalendarStart.get(Calendar.DAY_OF_YEAR) ==
                                            mCalendarEnd.get(Calendar.DAY_OF_YEAR)
                                            && mCalendarStart.get(Calendar.YEAR) ==
                                            mCalendarEnd.get(Calendar.YEAR)
                                    // now format the dates
                                    if (isSameDay) {
                                        bean.dateStart = mTimeFormatter.format(dtStart)
                                        bean.dateEnd = mTimeFormatter.format(dtEnd)
                                    } else {
                                        bean.dateStart = mDateFormatter.format(dtStart)
                                        bean.dateEnd = mDateFormatter.format(dtEnd)
                                    }
                                }

                                bean.title = eventsCursor.getString(1)
                                val description: String? = eventsCursor.getString(2)
                                if (description != null) {
                                    bean.description = description.substringBefore("place:").trim()
                                }
                                bean.location = if (eventsCursor.getString(3) != null)
                                    eventsCursor.getString(3).trim { it <= ' ' }
                                else
                                    null
                                bean.source = eventsCursor.getString(4)
                                bean.color = eventsCursor.getInt(5)
                                emitter.onNext(bean)

                                eventsCursor.close()
                                index++
                            }
                        }
                    }
                    emitter.onComplete()
                }
                        .subscribeOn(Schedulers.io())
            } else {
                return Observable.just(CalendarBean(name, CalendarBean.ID_SEQUENCE,
                        mResources.getString(R.string.provider_calendar))
                        .toPermissionsRequestBean())
            }
        }
        return Observable.empty()
    }

    override fun isCacheValid(dateBuilder: DateBuilder) = true
}