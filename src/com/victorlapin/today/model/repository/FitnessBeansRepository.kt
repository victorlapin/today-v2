package com.victorlapin.today.model.repository

import android.Manifest
import android.annotation.SuppressLint
import android.util.Log
import com.google.android.gms.fitness.HistoryClient
import com.google.android.gms.fitness.data.DataSet
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.data.Value
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.tasks.Tasks
import com.victorlapin.today.R
import com.victorlapin.today.manager.AccountManager
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.manager.SettingsManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.OAuthRequestBean
import com.victorlapin.today.model.bean.StepsBean
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

class FitnessBeansRepository(
        private val mSettings: SettingsManager,
        private val mResources: ResourcesManager,
        private val mAccounts: AccountManager
) : BeansRepository {
    private fun hasPermission() =
            mAccounts.hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)

    override val name = "fitness"

    @SuppressLint("MissingPermission")
    override fun provideBeans(dateBuilder: DateBuilder): Observable<Bean> {
        if (mSettings.isProviderEnabled(name) && mAccounts.isGoogleSignedIn()) {
            if (mAccounts.hasGooglePermission(mAccounts.fitnessOptions)) {
                if (hasPermission()) {
                    return Observable.create<Bean> { emitter ->
                        val client: HistoryClient? = mAccounts.getFitnessHistoryClient()
                        client?.let {
                            var steps = 0
                            var distance = 0f
                            var calories = 0f
                            var dataSet: DataSet?
                            var value: Value

                            val readRequest = DataReadRequest.Builder()
                                    .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                                    .aggregate(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA)
                                    .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                                    .bucketByTime(1, TimeUnit.DAYS)
                                    .setTimeRange(dateBuilder.today, dateBuilder.tomorrow, TimeUnit.MILLISECONDS)
                                    .build()

                            try {
                                val task = it.readData(readRequest)
                                val readResult = Tasks.await(task, mSettings.timeout.toLong(),
                                        TimeUnit.SECONDS)

                                if (task.isSuccessful) {
                                    val bucket = readResult.buckets[0]
                                    dataSet = bucket.getDataSet(DataType.TYPE_STEP_COUNT_DELTA)
                                    dataSet?.let {
                                        if (!it.isEmpty) {
                                            value = it.dataPoints[0].getValue(Field.FIELD_STEPS)
                                            if (value.isSet) steps = value.asInt()
                                        }
                                    }

                                    dataSet = bucket.getDataSet(DataType.TYPE_DISTANCE_DELTA)
                                    dataSet?.let {
                                        if (!it.isEmpty) {
                                            value = it.dataPoints[0].getValue(Field.FIELD_DISTANCE)
                                            if (value.isSet) distance = value.asFloat()
                                        }
                                    }

                                    dataSet = bucket.getDataSet(DataType.TYPE_CALORIES_EXPENDED)
                                    dataSet?.let {
                                        if (!it.isEmpty) {
                                            value = it.dataPoints[0].getValue(Field.FIELD_CALORIES)
                                            if (value.isSet) calories = value.asFloat()
                                        }
                                    }

                                    val bean = StepsBean(name,
                                            mResources.getString(R.string.provider_fitness))
                                    bean.source = "Google Fit"
                                    bean.lastUpdateTime = DateBuilder.formatter.format(Date())
                                    bean.steps = steps
                                    bean.distance = distance / 1000 // kilometers
                                    bean.calories = calories

                                    emitter.onNext(bean)
                                } else {
                                }
                            } catch (ignore: TimeoutException) {
                                Log.d("Fitness", "Fitness history timed out")
                            } catch (ignore: InterruptedException) {
                                Log.d("Fitness", "Fitness history request interrupted")
                            }
                        }
                        emitter.onComplete()
                    }
                            .subscribeOn(Schedulers.io())
                } else {
                    return Observable.just(
                            StepsBean(name,
                                    mResources.getString(R.string.provider_fitness))
                                    .toPermissionsRequestBean())
                }
            } else {
                return Observable.just(OAuthRequestBean(name, StepsBean.ID,
                        Bean.BEAN_TYPE_STEPS, mResources.getString(R.string.provider_fitness)))
            }
        }
        return Observable.empty()
    }

    override fun isCacheValid(dateBuilder: DateBuilder) = true
}