package com.victorlapin.today.model.repository

import android.Manifest
import android.annotation.SuppressLint
import android.net.Uri
import android.provider.CallLog
import com.victorlapin.today.R
import com.victorlapin.today.manager.AccountManager
import com.victorlapin.today.model.DateBuilder
import com.victorlapin.today.model.bean.Bean
import com.victorlapin.today.model.bean.CallsBean
import com.victorlapin.today.manager.ResourcesManager
import com.victorlapin.today.manager.SettingsManager
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.text.DateFormat
import java.text.SimpleDateFormat

class CallsBeansRepository(
        private val mSettings: SettingsManager,
        private val mResources: ResourcesManager,
        private val mAccounts: AccountManager
) : BeansRepository {
    private fun hasPermission() = mAccounts.hasPermission(Manifest.permission.READ_CALL_LOG)
    private val mDateFormatter =
            SimpleDateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.SHORT)
    private val mCallsLogProjection = arrayOf(
            CallLog.Calls._ID, // 0
            CallLog.Calls.NUMBER, // 1
            CallLog.Calls.DATE, // 2
            CallLog.Calls.DURATION, // 3
            CallLog.Calls.TYPE, // 4
            CallLog.Calls.CACHED_NAME, // 5
            CallLog.Calls.CACHED_FORMATTED_NUMBER, // 6
            CallLog.Calls.CACHED_PHOTO_URI, // 7
            CallLog.Calls.CACHED_NUMBER_TYPE, // 8
            CallLog.Calls.CACHED_LOOKUP_URI) // 9
    private val mUnknownNumberString = mResources.getString(R.string.provider_calls_unknown_number)

    override val name = "calls"

    @SuppressLint("MissingPermission")
    override fun provideBeans(dateBuilder: DateBuilder): Observable<Bean> {
        if (mSettings.isProviderEnabled(name)) {
            if (hasPermission()) {
                return Observable.create<Bean> { emitter ->
                    val callsCursor = mResources.resolver.query(CallLog.Calls.CONTENT_URI,
                            mCallsLogProjection,
                            CallLog.Calls.DATE + " >= ?",
                            arrayOf(dateBuilder.today.toString()),
                            CallLog.Calls.DATE + " DESC")
                    callsCursor.use { cursor ->
                        if (cursor.moveToFirst()) {
                            val count = mSettings.callsCount
                            val allowedTypes = mSettings.callsTypes
                            val bean = CallsBean(name,
                                    mResources.getString(R.string.provider_calls))
                            do {
                                val phone = cursor.getString(1)
                                val date = cursor.getLong(2)
                                val duration = cursor.getLong(3)
                                val type = cursor.getInt(4)
                                val name = cursor.getString(5)
                                val formattedPhone = cursor.getString(6)
                                val photoUri = cursor.getString(7)
                                val numberType = cursor.getInt(8)
                                val contactUri = cursor.getString(9)

                                val log = CallsBean.LogItem()
                                when {
                                    name != null && name.isNotEmpty() -> log.name = name
                                    formattedPhone != null && formattedPhone.isNotEmpty() ->
                                        log.name = formattedPhone
                                    phone != null && phone.isNotEmpty() -> log.name = phone
                                    else -> log.name = mUnknownNumberString
                                }
                                log.date = mDateFormatter.format(date)

                                when (type) {
                                    CallLog.Calls.REJECTED_TYPE,
                                    CallLog.Calls.INCOMING_TYPE ->
                                        log.type = CallsBean.CALL_TYPE_INCOMING
                                    CallLog.Calls.OUTGOING_TYPE ->
                                        log.type = CallsBean.CALL_TYPE_OUTGOING
                                    CallLog.Calls.MISSED_TYPE ->
                                        log.type = CallsBean.CALL_TYPE_MISSED
                                }

                                log.duration = "${duration / 3600}:${"%02d".format((duration % 3600) / 60)}:${"%02d".format(duration % 60)}"
                                if (photoUri != null) {
                                    log.photoUri = Uri.parse(photoUri)
                                }
                                log.numberType = numberType
                                if (contactUri != null) {
                                    log.contactUri = Uri.parse(contactUri)
                                }

                                if (allowedTypes.contains(log.type)) {
                                    bean.items.add(log)
                                }
                            } while (cursor.moveToNext() && bean.items.size < count)
                            emitter.onNext(bean)
                        }
                    }
                    emitter.onComplete()
                }
                        .subscribeOn(Schedulers.io())
            } else {
                return Observable.just(CallsBean(name,
                        mResources.getString(R.string.provider_calls))
                        .toPermissionsRequestBean())
            }
        }
        return Observable.empty()
    }

    override fun isCacheValid(dateBuilder: DateBuilder) = true
}