package com.victorlapin.today.model

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DateBuilder {
    val now: Long
    val today: Long
    val tomorrow: Long

    init {
        val cal = Calendar.getInstance()
        now = cal.time.time
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        today = cal.time.time
        cal.add(Calendar.DAY_OF_MONTH, 1)
        cal.add(Calendar.MINUTE, -1)
        tomorrow = cal.time.time
    }

    companion object {
        var formatter: DateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM,
                DateFormat.SHORT, Locale.getDefault())
    }
}