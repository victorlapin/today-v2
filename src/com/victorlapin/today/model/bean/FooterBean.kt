package com.victorlapin.today.model.bean

class FooterBean : Bean("footer", FooterBean.ID, "footer") {
    init {
        isDataBean = false
    }

    override val beanType = Bean.BEAN_TYPE_FOOTER

    companion object {
        const val ID = 9999L
    }
}