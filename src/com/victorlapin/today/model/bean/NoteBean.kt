package com.victorlapin.today.model.bean

class NoteBean(provider: String, beanId: Long, header: String) :
        Bean(provider, beanId, header) {

    var noteId: Long? = null
    var text: String? = null

    override val beanType = Bean.BEAN_TYPE_NOTES

    companion object {
        const val ID_SEQUENCE = 100L
    }
}