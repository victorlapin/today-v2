package com.victorlapin.today.model.bean

import java.util.*

class CurrenciesBean(provider: String, header: String) :
        Bean(provider, CurrenciesBean.ID, header) {

    var lastUpdateTime: String? = null
    val items = ArrayList<String>()

    override val beanType = Bean.BEAN_TYPE_CURRENCIES

    companion object {
        const val ID = 400L
    }
}
