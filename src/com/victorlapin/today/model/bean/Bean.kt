package com.victorlapin.today.model.bean

abstract class Bean protected constructor(val providerName: String,
                                          val id: Long, val cardHeader: String) :
        Comparable<Bean> {

    var title: String? = null
    var source: String? = null
    var isDataBean = true
    var isPolymorphBean = false
    var excludeFromAssistant = false

    abstract val beanType: Int

    override fun compareTo(other: Bean) = (this.id - other.id).toInt()

    open fun getRequiredPermissions(): Array<String>? = null

    fun toPermissionsRequestBean() =
        this as? PermissionsRequestBean ?:
                PermissionsRequestBean(providerName, id,
                        beanType, cardHeader, getRequiredPermissions())

    companion object {
        const val BEAN_TYPE_TOOLBAR = 10
        const val BEAN_TYPE_WEATHER = 11
        const val BEAN_TYPE_CURRENCIES = 12
        const val BEAN_TYPE_CALENDAR = 13
        const val BEAN_TYPE_CRYPTO_CURRENCIES = 14
        const val BEAN_TYPE_WEARABLES = 15
        const val BEAN_TYPE_NOTES = 16
        const val BEAN_TYPE_SEARCH = 17
        const val BEAN_TYPE_STEPS = 18
        const val BEAN_TYPE_CALLS = 19
        const val BEAN_TYPE_HEADER = 20
        const val BEAN_TYPE_SIGN_IN_GOOGLE = 21
        const val BEAN_TYPE_PERMISSIONS = 22
        const val BEAN_TYPE_PERMISSIONS_OAUTH = 23
        const val BEAN_TYPE_GREETING = 24
        const val BEAN_TYPE_NOTES_PLACEHOLDER = 25
        const val BEAN_TYPE_FOOTER = 99
    }
}