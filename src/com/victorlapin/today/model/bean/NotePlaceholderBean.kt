package com.victorlapin.today.model.bean

class NotePlaceholderBean(provider: String, beanId: Long, header: String) :
        Bean(provider, beanId, header) {

    var text: String? = null

    override val beanType = Bean.BEAN_TYPE_NOTES_PLACEHOLDER
}