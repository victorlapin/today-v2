package com.victorlapin.today.model.bean

class PermissionsRequestBean(parentProvider: String, beanId: Long, val parentType: Int,
                             parentHeader: String,
                             val permissions: Array<String>?
): Bean(parentProvider, beanId, parentHeader) {
    init {
        isPolymorphBean = true
    }

    override val beanType = BEAN_TYPE_PERMISSIONS
}