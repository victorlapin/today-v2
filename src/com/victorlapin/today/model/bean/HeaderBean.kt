package com.victorlapin.today.model.bean

class HeaderBean : Bean("header", HeaderBean.ID, "header") {
    init {
        isDataBean = false
    }

    override val beanType = Bean.BEAN_TYPE_HEADER

    companion object {
        const val ID = 0L
    }
}