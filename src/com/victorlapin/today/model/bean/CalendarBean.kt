package com.victorlapin.today.model.bean

import android.Manifest

class CalendarBean(provider: String, beanId: Long, header: String) :
        Bean(provider, beanId, header) {

    var color: Int? = null
    var dateStart: String? = null
    var dateEnd: String? = null
    var description: String? = null
    var location: String? = null
    var isAllDayEvent: Boolean? = null
    var eventId: Long? = null

    override val beanType = Bean.BEAN_TYPE_CALENDAR

    override fun getRequiredPermissions() = arrayOf(Manifest.permission.READ_CALENDAR)

    companion object {
        const val ID_SEQUENCE = 150L
    }
}
