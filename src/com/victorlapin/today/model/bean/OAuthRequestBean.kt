package com.victorlapin.today.model.bean

class OAuthRequestBean(parentProvider: String, beanId: Long, val parentType: Int,
                             parentHeader: String
): Bean(parentProvider, beanId, parentHeader) {
    init {
        isPolymorphBean = true
    }

    override val beanType = BEAN_TYPE_PERMISSIONS_OAUTH
}