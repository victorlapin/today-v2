package com.victorlapin.today.model.bean

import java.util.*

class WearablesBean(provider: String, header: String) :
        Bean(provider, WearablesBean.ID, header) {

    val devices = ArrayList<String>()

    override val beanType = Bean.BEAN_TYPE_WEARABLES

    companion object {
        const val ID = 600L
    }
}
