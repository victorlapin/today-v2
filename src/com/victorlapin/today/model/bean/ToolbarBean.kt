package com.victorlapin.today.model.bean

class ToolbarBean :
        Bean("toolbar", ToolbarBean.ID, "toolbar") {
    init {
        isDataBean = false
        excludeFromAssistant = true
    }

    override val beanType = Bean.BEAN_TYPE_TOOLBAR

    companion object {
        const val ID = 1L
    }
}