package com.victorlapin.today.model.bean

class SearchBean(hint: String, val url: String, val appPackage: String) :
        Bean("search", SearchBean.ID, "search") {

    init {
        isDataBean = false
        title = hint
    }

    override val beanType = Bean.BEAN_TYPE_SEARCH

    companion object {
        const val ID = 10L
        const val GOOGLE = 0
        const val DUCKDUCKGO = 1
        const val BING = 2
        const val YANDEX = 3
        const val EXTRA_URL = "EXTRA_URL"
        const val EXTRA_APP_PACKAGE = "EXTRA_APP_PACKAGE"
        const val EXTRA_QUERY = "EXTRA_QUERY"
    }
}
