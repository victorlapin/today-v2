package com.victorlapin.today.model.bean

import java.util.ArrayList

class CryptoCurrenciesBean(provider: String, header: String) :
        Bean(provider, CryptoCurrenciesBean.ID, header) {

    data class Item (
        val value: String,
        val imageRes: Int? = null
    )

    var lastUpdateTime: String? = null
    val items = ArrayList<Item>()

    override val beanType = Bean.BEAN_TYPE_CRYPTO_CURRENCIES

    companion object {
        const val ID = 410L
    }
}