package com.victorlapin.today.util

import android.content.Context
import android.graphics.Point
import android.util.DisplayMetrics
import android.view.WindowManager
import com.victorlapin.today.App

object Dimensions {
    private fun getDimension(resourceName: String): Int {
        val res = App.instance.resources
        var result = 0
        val resourceId = res.getIdentifier(resourceName, "dimen", "android")
        if (resourceId > 0) {
            result = res.getDimensionPixelSize(resourceId)
        }
        return result
    }

    val navigationBarHeight = getDimension("navigation_bar_height")

    val statusBarHeight = getDimension("status_bar_height")

    fun hasSoftwareKeys(context: Context): Boolean {
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay

        val realDisplayMetrics = DisplayMetrics()
        display.getRealMetrics(realDisplayMetrics)
        val realHeight = realDisplayMetrics.heightPixels

        val displayMetrics = DisplayMetrics()
        display.getMetrics(displayMetrics)
        val displayHeight = displayMetrics.heightPixels

        return realHeight - displayHeight > 0
    }

    fun getScreenHeight(context: Context): Int {
        // return screen height for smooth scrolling
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        wm.defaultDisplay.getSize(size)
        return size.y
    }
}